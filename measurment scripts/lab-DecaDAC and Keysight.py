# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 09:08:52 2021

@author: levon
"""
import time
from pathlib import Path
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
from qcodes.instrument_drivers.Harvard.Decadac import Decadac
from qcodes.instrument_drivers.Keysight.Keysight_34461A_submodules import Keysight_34461A
# from qcodes.instrument_drivers.stanford_research.SR830 import SR830
import os
from qcodes.tests.instrument_mocks import DummyInstrument

from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot
#matplotlib inline
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

import numpy as np
from tqdm import tqdm

from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
from qcodes.dataset.experiment_container import new_experiment

from qcodes.instrument.specialized_parameters import ElapsedTimeParameter
# t0=time.time()
# def experiment_time(t0=t0):
#       t= round((time.time()-t0),4)
#       return t
#%% Load all instruments
station = Station()
qc.Instrument.close_all()
station = qc.Station()
dac = Decadac('dac', 'ASRL6::INSTR', min_val=-10, max_val=10,terminator='\n')

dmm1 = Keysight_34461A('dmm1', 'TCPIP0::169.254.40.10::inst0::INSTR')
dmm2 = Keysight_34461A('dmm2', 'TCPIP0::169.254.40.9::inst0::INSTR')
dmm1.reset()
dmm2.reset()
#%%
"""Reset validators for DAC to fit to our model"""
for chan in enumerate(dac.channels):     
      #important!
      if chan[0]<=3:
            chan[1].update_period.set(50)
      else:
            chan[1].update_period.set(1000)
      
      chan[1].enable_ramp.set(False)
      chan[1].volt(0)

#%% Set location here

Expname="4K with IV converter: Saturation mode measurments and TTs"
Sampname="3-F5"
p = Path(time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+"/%d-%m-%Y"))
Path(p).mkdir(parents=True, exist_ok=True)
initialise_or_create_database_at(time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/%d-%m-%Y at %H_%M_%S o'clock.db" ))
exp = load_or_create_experiment(experiment_name=Expname , sample_name=Sampname)

os.chdir(p)

#Additonally for loops
location=time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/#{counter}_{time}_{name}")
loc_provider = qc.data.location.FormatLocation(fmt=location)
qc.data.data_set.DataSet.location_provider=loc_provider

g1= ManualParameter('Emitter_amplification_of_IV_converter', initial_value=1e05)
g2= ManualParameter('Base_amplification_of_IV_converter', initial_value=1e05)

station.add_component(g1)
station.add_component(g2)
station.add_component(dac)
station.add_component(dmm1)
station.add_component(dmm2)

#%% Define Values
emitter_device = dac.channels[0]
basis_device = dac.channels[1]
emitter_channel = dac.channels[0] 
basis_channel = dac.channels[1]

# Emitter Parameterss
V_Emitter_Start = -995e-03 #-920e-03
V_Emitter_Ziel = -1020e-03 #-950e-03
npoints_Emitter = 50

# Basis Parameterssì
V_Basis_Start = -0e-03*100  # 100e-03
V_Basis_Ziel = -50e-03*100
npoints_Basis = 50

delay_time = 0.1

# lockin_freq = 74.25
# lockin_amp = 0.5

sweep_Emitter_range = [V_Emitter_Start, V_Emitter_Ziel]
sweep_Emitter_step = (sweep_Emitter_range[1] - sweep_Emitter_range[0]) /npoints_Emitter #(npoints_Emitter-1)
sweep_Basis_range = [V_Basis_Start, V_Basis_Ziel]
sweep_Basis_step = (sweep_Basis_range[1] - sweep_Basis_range[0])/npoints_Basis #(npoints_Basis-1)

#%% Setting measurement speed (in PLC units) and ranges
print(dmm1.line_frequency())
print(dmm2.line_frequency())
print(dmm1.NPLC())
print(dmm2.NPLC())
print(dmm1.range())
print(dmm2.range())
nplc=1
dmm1.NPLC(nplc)
dmm2.NPLC(nplc)
rate=0.1
# Range1=10
# Range2=0.1
# dmm1.range(Range1)
# dmm2.range(Range2)
#dmm1.aperture_time(2e-5)
#dmm2.aperture_time(2e-5)

#dmm1.sample.source('TIM')
# dmm1.sample.timer('MIN')
# dmm2.sample.source('TIM')
# dmm2.sample.timer('MIN')

# dmm1.display.enabled(False)
# dmm2.display.enabled(False)

# dmm1.print_readable_snapshot(update=True)
# dmm2.print_readable_snapshot(update=True)
#%% Initialize: Savely initiate to starting voltages
rate=0.1
print('initializing...')
time.sleep(1)
print('setting up V_Emitter...')
emitter_device.ramp(V_Emitter_Start, rate)
print('setting up V_Basis...')
basis_device.ramp(V_Basis_Start, rate)

for i in tqdm(range(20)):
      time.sleep(0.1)
      
#%% 1D sweep of Emitter
g1.set(1e05)
g2.set(1e05)
time.sleep(2)
sweep_Emitter = Loop(emitter_channel.volt.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=delay_time).each(dmm1.volt, emitter_channel.volt, basis_channel.volt, dmm2.volt)
data_Emitter = sweep_Emitter.get_data_set(name="Emitter_Sweep" )
plot = QtPlot()
plot.add(data_Emitter.dmm1_volt, subplot=1)
plot.add(data_Emitter.dmm2_volt, subplot=1)
sweep_Emitter.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Emitter_Sweep at %H_%M_%S o'clock.png"))

#%% 1D sweep of Basis
g1.set(1e05)
g2.set(1e05)
time.sleep(2)
sweep_Basis =Loop(basis_channel.volt.sweep(sweep_Basis_range[0],sweep_Basis_range[1],sweep_Basis_step), delay=delay_time).each(dmm1.volt, emitter_channel.volt, basis_channel.volt, dmm2.volt)
data_Basis = sweep_Basis.get_data_set(name="Basis Sweep")
plot = QtPlot()
plot.add(data_Basis.dmm1_volt, subplot=1)
plot.add(data_Basis.dmm2_volt, subplot=1)
sweep_Basis.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Basis Sweep at %H_%M_%S o'clock.png"))

#%% 2D sweep
g1.set(1e05)
g2.set(1e09)
loop_2d = Loop(emitter_channel.volt.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=2*delay_time).loop(basis_channel.volt.sweep(sweep_Basis_range[0],sweep_Basis_range[1], sweep_Basis_step), delay=delay_time).each(dmm1.volt, emitter_channel.volt, basis_channel.volt, dmm2.volt)
data_2d = loop_2d.get_data_set(name="2D_Sweep")
plot= QtPlot()
plot.add(data_2d.dmm1_volt, subplot=1,name="Collector current")
plot.add(data_2d.dmm2_volt, subplot=2,name="Basis current")
loop_2d.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("2D sweep at %H_%M_%S o'clock.png"))
#%% Set and Sense with time trace of Emitter and Collector
g1.set(1e05)
g2.set(1e09)
n_samples=2000
VeSS=-1010e-03
VbSS=-10e-03*100
wait_time = 1
emitter_channel.volt(VeSS)
basis_channel.volt(VbSS)

#%% continuation
dmm1.range(1)
dmm2.range(10)
emitter_device.volt.set(VeSS)
basis_device.volt.set(VbSS)
Time=ElapsedTimeParameter('Time')
#Time.reset_clock() not necessary
meas=Measurement(exp=exp,station=station, name="Set and Sense" )
meas.register_parameter(Time)
meas.register_parameter(emitter_channel.volt, setpoints=[Time])
meas.register_parameter(basis_channel.volt, setpoints=[Time])
meas.register_parameter(dmm1.volt, setpoints=[Time])
meas.register_parameter(dmm2.volt, setpoints=[Time])
meas.write_period = 10 #default is 5 seconds
dmm1.display.enabled(False)
dmm2.display.enabled(False)
with meas.run(write_in_background=True) as datasaver:
      for i in tqdm(range(n_samples)):
          readout_V1 = emitter_channel.volt()
          readout_V2 = basis_channel.volt()
          readout_I1 = dmm1.volt()
          readout_I2 = dmm2.volt()
          now = Time()
          datasaver.add_result((emitter_device.volt, readout_V1),(basis_device.volt, readout_V2),(dmm1.volt, readout_I1),(dmm2.volt, readout_I2), (Time, now))
          time.sleep(wait_time)
        
dataset= datasaver.dataset
dmm1.display.enabled(True)
dmm2.display.enabled(True)
fig, ax = plot_dataset(dataset)
fig[2].figure.savefig(fname="Set an d Sense with time trace of Collector current at time.strftime('%H_%M_%S') o'clock.png")
fig[3].figure.savefig(fname="Set and Sense with time trace of Basis current at time.strftime('%H_%M_%S') o'clock.png")
#%% Ramping to 0 in a controlled manner
for channel in dac.channels:channel.volt(0)
#%% Set and Sense with time trace of Emitter and Base v2
g1.set(1e05)
g2.set(1e09)
VeSS=-1010e-03
VbSS=-10e-03*100
emitter_channel.volt(VeSS)
basis_channel.volt(VbSS)
#%% Set and Sense v2
dmm1.range(1)
dmm2.range(10)

npts=10000
nplc=0.02
dt=0

print(f'Minimal allowable dt: {dmm1.sample.timer_minimum()} s')
print(f'Minimal allowable dt: {dmm2.sample.timer_minimum()} s')

meas = Measurement(exp=exp,station=station, name="Set and Sense" )
meas.register_parameter(dmm1.timetrace)
meas.register_parameter(dmm2.timetrace)

dmm1.NPLC(nplc)
dmm1.timetrace_dt(dt)
dmm1.timetrace_npts(npts)
dmm2.NPLC(nplc)
dmm2.timetrace_dt(dt)
dmm2.timetrace_npts(npts)

dmm1.display.enabled(False)
dmm2.display.enabled(False)
dmm1.autozero(False)
dmm2.autozero(False)
with meas.run(write_in_background=True) as datasaver:
    datasaver.add_result((dmm1.timetrace, dmm1.timetrace()),
                          (dmm1.time_axis, dmm1.time_axis()),
                          (dmm2.timetrace, dmm2.timetrace()),
                          (dmm2.time_axis, dmm2.time_axis()))
time_trace_ds = datasaver.dataset
axs, cbs = plot_dataset(time_trace_ds)
dmm1.display.enabled(True)
dmm2.display.enabled(True)
fig[2].figure.savefig(fname="Set an d Sense with time trace of Collector current at time.strftime('%H_%M_%S') o'clock.png")
fig[3].figure.savefig(fname="Set and Sense with time trace of Basis current at time.strftime('%H_%M_%S') o'clock.png")
#%% Ramping to 0 in a controlled manner
for channel in dac.channels:channel.volt(0)
#%%
'''
# #%% Set and Sense v3: buffer
# VeSS=-940e-03
# VbSS=-0e-03*100
# emitter_device.ramp(VeSS, rate)
# basis_device.ramp(VbSS, rate)

# n_samples=10
# dmm1.autorange_once()
# dmm2.autorange_once()
# nplc=10
# dmm1.NPLC(nplc)
# dmm2.NPLC(nplc)
# # dmm1.aperture_mode('ON')
# # dmm1.aperture_time(2e-5)
# # dmm2.aperture_mode('ON')
# # dmm2.aperture_time(2e-5)
# dmm1.trigger.source('IMM')
# dmm1.trigger.count(1)
# dmm1.trigger.delay(0.0)
# dmm2.trigger.source('IMM')
# dmm2.trigger.count(1)
# dmm2.trigger.delay(0.0)
# # dmm1.sample.count(n_samples)
# # dmm1.sample.pretrigger_count(0)
# # dmm2.sample.count(n_samples)
# # dmm2.sample.pretrigger_count(0)
# # dmm1.sample.source('TIM')
# # dmm1.sample.timer('MIN')
# # dmm2.sample.source('TIM')
# # dmm2.sample.timer('MIN')
# dmm1.display.enabled(False)
# dmm2.display.enabled(False)

# dmm1.init_measurement()
# dmm2.init_measurement()
# n_samples1 = dmm1.sample.count()
# n_samples2 = dmm2.sample.count()
# time_per_sample1 = dmm1.sample.timer()
# time_per_sample2 = dmm2.sample.timer()
# old_timeout1 = dmm1.timeout()
# old_timeout2 = dmm2.timeout()
# new_timeout1 = old_timeout1 + n_samples1 * time_per_sample1
# new_timeout2 = old_timeout2 + n_samples2 * time_per_sample2
# with dmm1.timeout.set_to(new_timeout1) and dmm2.timeout.set_to(new_timeout2):
# data1 = dmm1.fetch()
# data2 = dmm2.fetch()
# dmm1.display.enabled(True)
# dmm2.display.enabled(True)
'''