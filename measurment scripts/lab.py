# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:28:54 2020

@author: levon
"""

import time
from pathlib import Path
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#from qcodes.instrument_drivers.Harvard.FZJ_Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
#from qcodes.instrument_drivers.agilent.Agilent_34400A import Agilent_34400A
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument

from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot
#matplotlib inline
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

import numpy as np
from tqdm import tqdm

from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
from qcodes.dataset.experiment_container import new_experiment

from qcodes.instrument.specialized_parameters import ElapsedTimeParameter
t0=time.time()
def experiment_time(t0=t0):
      t= round((time.time()-t0),4)
      return t
  
#% Load all instruments
qc.Instrument.close_all()
station = qc.Station()    
keithley1=Keithley2450('keithley1', 'GPIB1::11::INSTR')
keithley2=Keithley2450('keithley2', 'GPIB1::22::INSTR')
#station.add_component(keithley1)
#station.add_component(keithley2)
#agilent = Agilent_34400A('agilent', 'GPIB1::28::INSTR')
#lockin=SR830("lockin",'GPIB1::12::INSTR')

#counter=DummyInstrument(name="dummy2")
#counter.add_parameter('count', set_cmd=None)
from qcodes.logger.logger import start_all_logging
#start_all_logging() #a headache, avoid this
#%% Set location here

Expname="4K with current sourcing- Saturation mode measurments check"
Sampname="416L-L6-D3"
p = Path(time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+"/%d-%m-%Y"))
Path(p).mkdir(parents=True, exist_ok=True)
initialise_or_create_database_at(time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/%d-%m-%Y at %H_%M_%S o'clock.db" ))
exp = load_or_create_experiment(experiment_name=Expname , sample_name=Sampname)

os.chdir(p)

#Additonally for loops
location=time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/#{counter}_{time}_{name}")
loc_provider = qc.data.location.FormatLocation(fmt=location)
qc.data.data_set.DataSet.location_provider=loc_provider



#%% Define Values
keithley2.source_function('voltage')
keithley2.sense_function('current')
emitter_device = keithley1
basis_device = keithley2
basis_channel = basis_device.source.voltage
emitter_channel = emitter_device.source.voltage 

# Emitter Parameterss
V_Emitter_Start = -0e-03
V_Emitter_Ziel = -1100e-03
npoints_Emitter = 50

# Basis Parameterss
V_Basis_Start = 0e-03  # 100e-03
V_Basis_Ziel = -50e-03
npoints_Basis = 50

delay_time = 0.1

# lockin_freq = 74.25
# lockin_amp = 0.5

sweep_Emitter_range = [V_Emitter_Start, V_Emitter_Ziel]
sweep_Emitter_step = (sweep_Emitter_range[1] - sweep_Emitter_range[0])/npoints_Emitter #(npoints_Emitter-1)
sweep_Basis_range = [V_Basis_Start, V_Basis_Ziel]
sweep_Basis_step = (sweep_Basis_range[1] - sweep_Basis_range[0])/npoints_Basis #(npoints_Basis-1)
#%% Setting measurement speed (in PLC units) and ranges
print(keithley1.line_frequency.get())
print(keithley2.line_frequency.get())
print(keithley1.sense.nplc.get())
print(keithley2.sense.nplc.get())
print(keithley1.source.range.get())
print(keithley2.source.range.get())
nplc=1
keithley1.sense.nplc.set(nplc)
keithley2.sense.nplc.set(nplc)

# RangeV_k1=2
# # RangeV_k2=200e-03
# keithley1.source.range.set(RangeV_k1)
# # keithley2.source.range.set(RangeV_k2)

# # RangeI_k1=1e-06
# RangeI_k2=10e-09
# # keithley1.sense.range.set(RangeI_k1)
# keithley2.sense.range.set(RangeI_k2)

keithley1.source.read_back_enabled(False)
keithley2.source.read_back_enabled(False)

keithley1.output_enabled(True)
keithley2.output_enabled(True)
# keithley1.print_readable_snapshot()
# keithley2.print_readable_snapshot()
#%%Define ramp_keithley
def ramp_keithley(value, step=0.1, wait=0.01, device=keithley1):
      
      if not device.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device.source_function.get()=='voltage':
            print("Ramping only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      
      try:  
            if device.source.voltage.get()==value:
                  print("Target value already reached")
                  return True
            assert step > 0
            if device.source.voltage.get()>value:
                  step*=-1                  
            
            set_value=device.source.voltage.get()
            while set_value<value-np.abs(step) or set_value>value+np.abs(step):
                  set_value+=step
                  device.source.voltage.set(set_value)           
                  time.sleep(wait)

            device.source.voltage.set(value)
            
            return True
      except:
            print("Something went wrong")
#%% Initialize
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Voltage = basis_channel.get()

# savely initiate to starting voltages
print('initializing...')
time.sleep(1)
print('ramping up V_Emitter...')
ramp_keithley(V_Emitter_Start, np.abs(current_Emitter_Voltage - V_Emitter_Start)/20, delay_time, emitter_device)
print('ramping up V_Basis...')
ramp_keithley(V_Basis_Start, np.abs(current_Basis_Voltage - V_Basis_Start)/20, delay_time, basis_device)

for i in tqdm(range(20)):
      time.sleep(0.1)
      
#%% 1D sweep of Emitter
emitter_channel.set(V_Emitter_Start)
time.sleep(2)
sweep_Emitter = Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_Emitter = sweep_Emitter.get_data_set(name="Emitter_Sweep" )
plot = QtPlot()
plot.add(data_Emitter.keithley1_sense_current, subplot=1)
plot.add(data_Emitter.keithley2_sense_current, subplot=1)
sweep_Emitter.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Emitter_Sweep at %H_%M_%S o'clock.png"))

#%% 1D sweep of Basis
basis_channel.set(V_Basis_Start)
time.sleep(2)
sweep_Basis =Loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1],sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_Basis = sweep_Basis.get_data_set(name="Basis Sweep")
plot = QtPlot()
plot.add(data_Basis.keithley1_sense_current, subplot=1)
plot.add(data_Basis.keithley2_sense_current, subplot=1)
sweep_Basis.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Basis Sweep at %H_%M_%S o'clock.png"))

#%% 2D sweep
loop_2d = Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=2*delay_time).loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1], sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.current)
data_2d = loop_2d.get_data_set(name="2D_Sweep")
plot= QtPlot()
plot.add(data_2d.keithley1_sense_current, subplot=1,name="Emitter current")
plot.add(data_2d.keithley2_sense_current, subplot=2,name="Basis current")
loop_2d.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("2D sweep at %H_%M_%S o'clock.png"))

#%% Set and Sense with time trace of Emitter and Basis
n_measurements=1000
VeSS=-900e-03
VbSS=-10e-03
wait_time = 1
emitter_device.source.voltage.set(VeSS)
basis_device.source.voltage.set(VbSS)
Time=ElapsedTimeParameter('Time')
#Time.reset_clock() not necessary
meas=Measurement(exp=exp, name="Set and Sense" )
meas.register_parameter(Time)
meas.register_parameter(emitter_channel, setpoints=[Time])
meas.register_parameter(basis_channel, setpoints=[Time])
meas.register_parameter(keithley1.sense.current, setpoints=[Time])
meas.register_parameter(keithley2.sense.current, setpoints=[Time])
meas.write_period = 10 #default is 5 seconds
with meas.run(write_in_background=True) as datasaver:
      for i in tqdm(range(n_measurements)):
          readout_V1 = keithley1.source.voltage.get()
          readout_V2 = keithley2.source.voltage.get()
          readout_I1 = keithley1.sense.current.get()
          readout_I2 = keithley2.sense.current.get()
          now = Time()
          datasaver.add_result((emitter_device.source.voltage, readout_V1),(basis_device.source.voltage, readout_V2),(emitter_device.sense.current, readout_I1),(basis_device.sense.current, readout_I2), (Time, now))
          time.sleep(wait_time)
        
dataset= datasaver.dataset
fig, ax = plot_dataset(dataset)
fig[2].figure.savefig(fname="Set an d Sense with time trace of Emitter current at time.strftime('%H_%M_%S') o'clock.png")
fig[3].figure.savefig(fname="Set and Sense with time trace of Basis current at time.strftime('%H_%M_%S') o'clock.png")

#%% Set and Sense with buffer
keithley1.reset()
keithley2.reset()
nplc=0.01
keithley1.sense.nplc.set(nplc)
keithley2.sense.nplc.set(nplc)
keithley1.output_enabled(True)
keithley2.output_enabled(True)
VeSS=-900e-03
VbSS=-10e-03
basis_device.source.voltage.set(VbSS)
emitter_device.source.voltage.set(VeSS)
buffer_name = 'userbuff1'
n_measurements = 10000
buffer_size = 10000 #perhaps you must skip this line
with keithley1.buffer(buffer_name, buffer_size) as buff1:
    with keithley2.buffer(buffer_name, buffer_size) as buff2:
        buff1.elements(['relative_time', 'measurement', 'source_value'])
        buff2.elements(['relative_time', 'measurement', 'source_value'])
        keithley1.source.sweep_setup(VeSS, VeSS, n_measurements, buffer_name=buff1.buffer_name)
        keithley2.source.sweep_setup(VbSS, VbSS, n_measurements, buffer_name=buff2.buffer_name)
        for i in tqdm(range(100)):time.sleep(0.1)  
        data1 = keithley1.sense.sweep()
        all_data1 = keithley1.sense.sweep.get_selected()
        data2 = keithley2.sense.sweep()
        all_data2 = keithley2.sense.sweep.get_selected()
        print(data1)
        print(all_data1)
        print(data2)
        print(all_data2)



# result1=np.array(all_data1)
# result1.resize([n_measurements,3])
# with open('Set and Sense with buffer1.npy', 'wb') as f:
#     np.save(f, result1)
#     f.close()
# result2=np.array(all_data2)
# result2.resize([n_measurements,3])
# with open('Set and Sense with buffer2.npy', 'wb') as f:
#     np.save(f, result2)
#     f.close()


meas=Measurement(exp=exp, name="Set and Sense" )
meas.register_parameter(buff1.elements(["time"]))
meas.register_parameter(buff2.elements(["time"]))
meas.register_parameter(buff1.elements(["source_value"]))
meas.register_parameter(buff2.elements(["source_value"]))
meas.register_parameter(buff1.elements(["measurement"]))
meas.register_parameter(buff2.elements(["measurement"]))
with meas.run() as datasaver:
    Time1=buff1.elements(["time"])
    Time2=buff2.elements(["time"])
    datasaver.add_result((emitter_device.source.voltage, buff1.elements(["source_value"])),(basis_device.source.voltage, buff2.elements(["source_value"])),(emitter_device.sense.current, buff1.elements(["measurement"])),(basis_device.sense.current, buff2.elements(["measurement"])), (Time1, buff1.elements(["time"])), (Time2, buff2.elements(["time"])))        
dataset= datasaver.dataset
fig, ax = plot_dataset(dataset)
fig[2].figure.savefig(fname="Set an d Sense with time trace of Emitter current at time.strftime('%H_%M_%S') o'clock.png")
fig[3].figure.savefig(fname="Set and Sense with time trace of Basis current at time.strftime('%H_%M_%S') o'clock.png")
#%% Ramping to 0 in a controlled manner
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Voltage = basis_channel.get()
print('Ramping down...')
time.sleep(1)
print('ramping down V_Emitter...')
ramp_keithley(0, np.abs(current_Emitter_Voltage)/20, delay_time, emitter_device) #or keithley1.source.voltage.set(0)
print('ramping down V_Basis...')
ramp_keithley(0, np.abs(current_Basis_Voltage)/20, delay_time, basis_device) # or #or keithley2.source.voltage.set(0)

keithley1.output_enabled(False)
keithley2.output_enabled(False)

#%%Further Notes
'''
1)Use QTPlot for live plotting and MatPlot for data analyzing
2)interactive widget
3)with keithley.output_enabled.set_to(True)-->keithley1.output_enabled(True)
4)plot_1d
5)There is also a more human-readable version of the essential information->dac.print_readable_snapshot()
6)Nevertheless, DataSet also provides the following convenient methods:
DataSet.write_data_to_text_file
7)progress_interval (int, float) – show progress of the loop every x seconds.
    If provided here, will override any interval provided with the Loop definition. Defaults to None

'''



'''
#%%Time Trace of Emitter (for keithley 2600 series)
keithley1.timetrace_mode('current')
dt=1e-3                       # the default value is 1 ms
npts=500                      # the default value is 500
keithley1.timetrace_dt(dt)
keithley1.timetrace_npts(npts)
# initialise_database()
# new_experiment(name='time trace emitter', sample_name="Sampname")
timemeas=qc.Measurement.register_parameter(keithley1.timetrace)
with timemeas.run() as datasaver:

    somenumbers = keithley1.timetrace.get()
    datasaver.add_result((keithley1.timetrace, somenumbers), (keithley1.time_axis, keithley1.time_axis.get()))
data = datasaver.dataset
plot_dataset(data)

#%%Time Trace of Basis (for keithley 2600 series)
keithley1.timetrace_mode('current')
dt=1e-3                       # the default value is 1 ms
npts=500                      # the default value is 500
keithley2.timetrace_dt(dt)
keithley2.timetrace_npts(npts)
# initialise_database()
# new_experiment(name='time trace basis', sample_name="Sampname")
timemeas=qc.Measurement.register_parameter(keithley2.timetrace)
with timemeas.run() as datasaver:

    somenumbers = keithley2.timetrace.get()
    datasaver.add_result((keithley2.timetrace, somenumbers), (keithley2.time_axis, keithley1.time_axis.get()))
data = datasaver.dataset
plot_dataset(data)
fig, ax=plot_dataset(dataset)
fig[0].figure.savefig("{}, at {} o'clock.pdf".format(time.strftime('%d-%m-%Y'), time.strftime('%H_%M_%S')))
'''