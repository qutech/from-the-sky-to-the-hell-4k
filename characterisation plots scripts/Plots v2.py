# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 09:00:52 2021

@author: Baghumyan
"""
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#%matplotlib auto
import seaborn as sns
import math
 
#p="Y:/GaAs/Barumjan/DATA/HBT/416L-L3-D3/4K with grounding cable/22-12-2020/#011_14-15-54_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D3/4K with current sourcing- Saturation mode measurments check/09-06-2021/#008_08-36-14_2D_Sweep"
#p = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D4/4K with grounding cable/09-02-2021/#007_14-48-55_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"

dataset=qc.data.data_set.load_data(p)
print(dataset.read)

v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_source_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()*-1
i_b = dataset.keithley2_sense_current.ndarray.copy()
i_c=(i_e-i_b)

i_e[np.logical_or(i_b>500e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>500e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


# Pwr[np.where(Pwr>20000*1e-9)]=np.nan
# Beta[Beta==np.nanmax(Beta)]=np.nan

# mask=np.logical_and(i_b>=0.1e-9, i_b<=10e-9)
# A1=v_ec[mask]
# B1=v_bc[mask]
# A2=v_ec[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=v_be[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=np.sort(B2)

#%% design global parameters
cb_colour="viridis_r"
n_points=51
#%% 𝐼_E(𝑉_EC ,𝑉_BC), 𝐼_B(𝑉_EC ,𝑉_BC). 
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=v_ec*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[0].set_xlabel(r"$V_{EC}$ (mV)", size=20)
ax[1].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[1].set_xlabel(r"$V_{EC}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_B$ (nA)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝑉_𝐸𝐶 ,𝐼_𝐵), 𝑉_𝐵C(𝑉_𝐸𝐶 ,𝐼_𝐵).
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=i_b*1e9
Y=v_ec*1e03
Z1=i_e*1e9
Z2=v_bc*1e03
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_xlabel(r"$I_B$ (nA)", size=20)
ax[0].set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax[1].set_xlabel(r"$I_B$ (nA)", size=20)
ax[1].set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$V_{BC}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_E(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=i_e*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_E$ (nA)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% 𝐼_C(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=i_c*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_C$ (nA)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')

#%% g_m(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
Z=d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
Z[np.where(Z<0)]=np.nan
Y=v_ec*1e03
X=i_b*1e9
norm = mpl.colors.LogNorm(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$g_m $ (S)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% P(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=Pwr*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$P$ (nW)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝑉_BE ,𝑉_BC), 𝐼_B(𝑉_BE ,𝑉_BC). 
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=v_be*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[0].set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax[1].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[1].set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading='auto')
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_B$ (nA)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝐼_𝐵) for different 𝑉_𝐸𝐶 values
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$|I_E|$ (nA)", size=20)
ax.grid()
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=abs(i_e[n,:]*1e9)
    v=v_ec[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(-v_ec)*1e3, vmax=np.max(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#ax.set_ylabel(r"$\frac{dI_C}{dI_B}$", rotation=0, size=20)
ax.grid()
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x=i_b[n,:]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
# ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% diff 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"AC $\beta$", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_ylabel(r"$\frac{dI_C}{dI_B}$", rotation=0, size=20)
ax.grid()
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.set_yscale('log')
#%% 𝛽(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x=Pwr[n,]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(V_BE)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=20)
ax.set_xlabel(r"$V_{BE}$ (mV)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=v_be[n,1:-2]*1e3
    v=v_ec[n,0]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
'''
#analyzing
g=np.resize(d𝑔_m,(d𝑔_m.size,))
xx=np.resize(v_be*1e3,(d𝑔_m.size,))
indexs_to_order_BY = xx.argsort()
X_ordered = xx[indexs_to_order_BY]
Y_ordered = (g)[indexs_to_order_BY]
line_points=plt.ginput(2)
A=line_points[0]
B=line_points[1]
k=np.log(A[1]/B[1])/(A[0]-B[0])
x_points=X_ordered
y_points=np.exp(k*(x_points-A[0]))*A[1]
ax.plot(x_points,y_points)
mask1=y_points>=Y_ordered
ax.scatter(X_ordered[mask1],Y_ordered[mask1],color="k",marker="+")
# mask2[mask1]=False
#%%
Indi=Y_ordered[mask1] #indicator
Mask=np.zeros((51,51),dtype=bool)
for i in np.arange(0,51):
    for j in np.arange(0,51):
        for k in np.arange(Indi.size):
            if Indi[k]==dg_m[i,j]:
                Mask[i,j]=True
# v_ec[~Mask]=np.nan
# v_bc[~Mask]=np.nan
# v_be[~Mask]=np.nan
# i_e[~Mask]=np.nan
# i_b[~Mask]=np.nan
# i_c[~Mask]=np.nan
# Pwr[~Mask]=np.nan
#%%
fi2g,ax2 = plt.subplots(figsize=(12,12))
V_ec = dataset.keithley1_source_voltage.ndarray*1e3
V_bc = dataset.keithley2_source_voltage.ndarray*1e3
V_be=V_bc-V_ec
df = pd.DataFrame(Mask, columns =V_bc[0,:], index =V_ec[:,0])
# df[r"$V_{BC}$ $(mV)$"]=np.linspace(np.max(v_bc*1e3),np.max(v_bc*1e3),51 )
sns.heatmap(df,ax==ax2,cmap="seismic")
plt.xlabel(r"$V_{BC}$ (mV)", size=20)
plt.ylabel(r"$V_{EC}$ (mV)", size=20)
'''
#%% 𝑟_𝜋(𝐼_𝐵), 𝑟_𝑜(𝐼_𝐶)
fig,ax = plt.subplots(1, 2, figsize=(16,8))
ax[0].set_xlabel(r"$I_B$ (nA)", size=20)
ax[0].set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax[1].set_xlabel(r"$I_B$ (nA)", size=20)
ax[1].set_ylabel(r"$r_o$ $(\Omega)$", size=20)
ax[0].grid()
ax[1].grid()
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.4, hspace=0.2)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax[0].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax[1].set_prop_cycle(color=[cm(1.*i/(NUM_COLORS)) for i in range(NUM_COLORS)])
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
r_o=-np.gradient(v_ec,axis=0)/np.gradient(i_c,axis=0)
n=0
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x1=i_b[n,]*1e9
    x2=i_b[n,:]*1e9
    indexs_to_order_by1 = x1.argsort()
    x_ordered1 = x1[indexs_to_order_by1]
    y_ordered1 = (r_pi[n,])[indexs_to_order_by1]
    mask=y_ordered1<0
    y_ordered1[mask]=np.nan
    ax[0].scatter(x_ordered1, y_ordered1, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    try: 
        y_ordered2 = (r_o[n,])[indexs_to_order_by2]
        mask=y_ordered2<0
        y_ordered2[mask]=np.nan
        ax[1].scatter(x_ordered2, y_ordered2, marker=".", lw=1)
        n+=1
    except: n+=1
# ax[0].legend(bbox_to_anchor=(0, -0.5),loc=3, ncol=7, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax[0].xaxis.set_major_formatter(formatter)
ax[1].xaxis.set_major_formatter(formatter)
ax[0].yaxis.set_major_formatter(formatter)
ax[1].yaxis.set_major_formatter(formatter)
sm1 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb1=plt.colorbar(sm1,ax=ax[0])
cb1.ax.set_title(r"$V_{CE}$ (mV)", size=20)
sm2 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec[1:-1,:])*1e3, vmax=np.nanmax(-v_ec[1:-1,:]*1e3)))
cb2=plt.colorbar(sm2,ax=ax[1])
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
ax[0].set_yscale('log')
ax[1].set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax[0].spines[axis].set_linewidth(2)
  ax[1].spines[axis].set_linewidth(2)
ax[0].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[0].tick_params(width=1.5, length=12, which="major",direction="in")
ax[1].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[1].tick_params(width=1.5, length=12, which="major",direction="in")
#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=20)
ax.set_xlabel(r"$P$ (nW)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=20)
ax.set_ylabel(r"$g_m $ (mS)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(None, xmax=400)
# ax.set_ylim([-0.5,0.5])
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=Pwr[n,1:-2]*1e9
    v=v_ec[n,0]*1e3
    X=x
    indexs_to_order_BY = X.argsort()
    X_ordered = X[indexs_to_order_BY]
    Y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_BY]*1e03
    mask=Y_ordered<0
    Y_ordered[mask]=np.nan
    ax.scatter(X_ordered, Y_ordered+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
# fig2, ax2 = plt.subplots(figsize=(8, 8),facecolor="firebrick")
# norm = mpl.colors.Normalize(𝑔__𝑚.min(), 𝑔__𝑚.max())
# p = ax2.pcolor(i_b*1e9, v_ec*1e3, 𝑔__𝑚, norm=norm, cmap='inferno', shading='nearest')
# ax2.set_xlabel(r"$I_B$ $(nA)$")
# ax2.set_ylabel(r"$V_{EC}$ $(mV)$")
# cb = fig.colorbar(p, ax=ax2)
# cb.ax.set_title(r"$g_m$ $(nS)$")
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
# ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(i_b)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=18)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=18)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=i_b[n,1:-2]*1e9
    v=v_ec[n,0]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.tick_params(labelsize=16)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑟_𝜋(Pwr), 𝑟_𝑜(Pwr)
fig,ax = plt.subplots(1, 2, figsize=(16,8))
ax[0].set_xlabel(r"$P$ (nW)", size=20)
ax[0].set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax[1].set_xlabel(r"$P$ (nW)", size=20)
ax[1].set_ylabel(r"$r_o$ $(\Omega)$", size=20)
ax[0].grid()
ax[1].grid()
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.4, hspace=0.2)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax[0].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
r_o=-np.gradient(v_ec,axis=0)/np.gradient(i_c,axis=0)
n=0
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x1=Pwr[n,:]*1e9
    x2=Pwr[n,:]*1e9
    indexs_to_order_by1 = x1.argsort()
    x_ordered1 = x1[indexs_to_order_by1]
    y_ordered1 = (r_pi[n,])[indexs_to_order_by1]
    mask=y_ordered1<0
    y_ordered1[mask]=np.nan
    ax[0].scatter(x_ordered1, y_ordered1, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    try: 
        y_ordered2 = (r_o[n,])[indexs_to_order_by2]
        mask=y_ordered2<0
        y_ordered2[mask]=np.nan
        ax[1].scatter(x_ordered2, y_ordered2, marker=".", lw=1)
        n+=1
    except: n+=1
# ax[0].legend(bbox_to_anchor=(0, -0.5),loc=3, ncol=7, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax[0].xaxis.set_major_formatter(formatter)
ax[1].xaxis.set_major_formatter(formatter)
ax[0].yaxis.set_major_formatter(formatter)
ax[1].yaxis.set_major_formatter(formatter)
sm1 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb1=plt.colorbar(sm1,ax=ax[0])
cb1.ax.set_title(r"$V_{CE}$ (mV)", size=20)
sm2 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec[1:-1,:])*1e3, vmax=np.nanmax(-v_ec[1:-1,:]*1e3)))
cb2=plt.colorbar(sm2,ax=ax[1])
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
ax[0].set_yscale('log')
ax[1].set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax[0].spines[axis].set_linewidth(2)
  ax[1].spines[axis].set_linewidth(2)
ax[0].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[0].tick_params(width=1.5, length=12, which="major",direction="in")
ax[1].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[1].tick_params(width=1.5, length=12, which="major",direction="in")
'''
#%% Pwr(I_B)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$P$ (nW)", size=20)
ax.grid()
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = 51
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax.grid()
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=Pwr[n,:]*1e9
    v=v_ec[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(-v_ec)*1e3, vmax=np.max(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
'''

'''
#%% 𝛽(𝐼_𝐵) and 𝑔_𝑚(𝐼_𝐵)
fig,ax1 = plt.subplots(figsize=(24,12))
ax1.set_xlabel(r"$I_B$ (nA)", size=20)
ax1.set_ylabel(r"$\beta$", rotation=0, size=20)
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.grid()
ax1.tick_params(labelsize=16)
ax2=ax1.twinx()
ax2.set_ylabel(r"$g_m $ (S)", size=20)
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax1.yaxis.set_major_formatter(formatter)
ax2.yaxis.set_major_formatter(formatter)
ax2.set_yscale('log')
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax1.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax2.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
dg_m[:,0]=np.nan
dg_m[:,-1]=np.nan
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_ec[n,0]*1e03
    x=i_b[n,:]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax1.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    y2_ordered = (d𝑔_𝑚[n,:])[indexs_to_order_by]
    ax2.scatter(x_ordered, y2_ordered, marker=".", lw=1, label=r"$V_{EC}$=%.1f (mV)" % v)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
'''