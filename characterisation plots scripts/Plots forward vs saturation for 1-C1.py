"""
Created on Mon Jun 28 10:45:01 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#
import seaborn as sns
import math
from pprint import pprint
 

p="C:/Users/levon/Desktop/DATA/HBT/1-C1/4K with grounding cables and with IV converter/12-04-2021/#004_17-39-46_2D_Sweep/dac_Slot0_Chan0_volt_set_dac_Slot0_Chan1_volt_set.dat"
Sampname=p[32:36]
Sampname=Sampname+"_comparison"
os.chdir(p[0:36]+"/Plots of forward vs saturation for 1-C1")

dataset=qc.data.data_set.load_data(p)
print(dataset.read)
v_ec = dataset.dac_Slot0_Chan0_volt.ndarray.copy()
v_bc = dataset.dac_Slot0_Chan1_volt.ndarray.copy()/100
i_c = dataset.dmm1_volt.ndarray.copy()*1e-7*-1
i_b = dataset.dmm2_volt.ndarray.copy()*1e-7*-1
i_e=i_c+i_b
i_e[np.logical_or(i_b>3e-9, i_b<0,)]=np.nan
i_b[np.logical_or(i_b>3e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
# Pwr[np.where(Pwr>100*1e-9)]=np.nan

p2 = "C:/Users/levon/Desktop/DATA/HBT/1-C1/4K with IV converter- Saturation mode measurments/26-06-2021/#001_16-35-04_2D_Sweep"
dataset2=qc.data.data_set.load_data(p2)
print(dataset2.read)
g12=dataset2.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g22=dataset2.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec2 = dataset2.qdac_chan01_v.ndarray.copy()
v_bc2 = dataset2.qdac_chan02_v.ndarray.copy()
i_c2 = dataset2.dmm1_volt.ndarray.copy()*1/g12*-1
i_b2 = dataset2.dmm2_volt.ndarray.copy()*1/g22*-1
i_e2=i_c2+i_b2
i_e2[np.logical_or(i_b2>3e-9, i_b2<0)]=np.nan
i_b2[np.logical_or(i_b2>3e-9, i_b2<0)]=np.nan
# i_b2[i_e==np.nanmin(i_e)]=np.nan
# i_e2[i_e==np.nanmin(i_e)]=np.nan
mask2=np.isnan(i_b2)
v_ec2[mask]=np.nan
v_bc2[mask]=np.nan
i_b2[mask]=np.nan
v_be2=v_bc2-v_ec2
Pwr2=np.abs(i_c2*v_ec2)+np.abs(i_b2*v_be2)
#%% design global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 + 1) / 2
    golden_ratio = 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
cb_colour="viridis_r"
n_points=51
cb_colour2="inferno"
size=11
labelsize=8
regime_size=8
point_size=1
tick_width=0.5
#%% 𝛽(I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"$\beta$", rotation=0,  size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
Beta=i_c/i_b
Beta2=i_c2/i_b2
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n>= 0:
    x=i_b[n,:]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax], location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')
cb.ax.tick_params(labelsize=labelsize)
cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=i_b2[n,:]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = Beta2[n,:][indexs_to_order_by2]
    mask2=y_ordered<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax], location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
cb2.ax.tick_params(labelsize=labelsize)
# ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_beta(I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_beta(I_B).png", bbox_inches='tight')
#%% diff 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"AC $\beta$", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1

NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta2=np.gradient(i_c2,axis=1)/np.gradient(i_b2,axis=1)
ax.set_ylim(0, ymax=1000) #(nA)
while n>= 0:
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax],location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=i_b2[n,1:-2]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = AC_Beta2[n,1:-2][indexs_to_order_by2]
    mask2=y_ordered2<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb2.ax.tick_params(labelsize=labelsize)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_AC_beta(I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_AC_beta(I_B).png", bbox_inches='tight')
#%% diff 𝛽(I_B) log scale
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"AC $\beta$", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta2=np.gradient(i_c2,axis=1)/np.gradient(i_b2,axis=1)
while n>= 0:
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax],location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=i_b2[n,1:-2]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = AC_Beta2[n,1:-2][indexs_to_order_by2]
    mask2=y_ordered2<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb2.ax.tick_params(labelsize=labelsize)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_AC_beta(I_B)_log.pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_AC_beta(I_B)_log.png", bbox_inches='tight')
#%% 𝑔_m(V_BE)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$V_\mathrm{BE}$ (mV)", size=size)
ax.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n>= 0:
    x=v_be[n,1:-2]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax],location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=v_be2[n,1:-2]*1e3
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb2.ax.tick_params(labelsize=labelsize)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_g_m(V_BE).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_g_m(V_BE).png", bbox_inches='tight')
#%% 𝑔_m(Pwr)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$P$ (nW)", size=size)
ax.set_ylabel(r"$g_\mathrm{m} $ (mS)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n>= 0:
    x=Pwr[n,1:-2]*1e9
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]*1e03
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax],location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=Pwr2[n,1:-2]*1e9
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]*1e03
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb2.ax.tick_params(labelsize=labelsize)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
# ax.set_xlim(left=0, right=800)
ax.set_ylim(bottom=0, top=0.25)
plt.savefig(Sampname+"_g_m(Pwr).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_g_m(Pwr).png", bbox_inches='tight')
#%% 𝑔_m(I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
n=n_points-1
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n>= 0:
    x=i_b[n,1:-2]*1e9
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", s=point_size)
    n-=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
cb=plt.colorbar(sm,ax=[ax],location='top', pad=0.07)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV), Forward regime", size=regime_size)
cb.ax.xaxis.set_ticks_position('bottom')

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS,0,-1)])
n=n_points-1
while n>= 0:
    x2=i_b2[n,1:-2]*1e9
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", s=point_size)
    n-=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax], location='bottom', pad=0.2)
cb2.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb2.ax.tick_params(labelsize=labelsize)
cb2.ax.set_title(r"$V_\mathrm{CE}$  (mV), Saturation regime", size=regime_size)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_g_m(I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_g_m(I_B).png", bbox_inches='tight')
