# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 15:45:16 2021

@author: Baghumyan
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from collections import Counter#%% define global parameters
#%%
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 - 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
size=11
labelsize=8
point_size=1
tick_width=0.5
#%% Traditional QCoDeS way
os.chdir("C:/Users/levon/Desktop")
p="C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable TTs with Keithleys/27-03-2021/27-03-2021 at 19_26_00 o_clock.db"
initialise_or_create_database_at(p)
from qcodes.dataset.experiment_container import experiments
print(experiments())
time.sleep(0.5)
id=input("Please enter the experiment run id that you wish to use for calculations \nid=")
dataset=qc.dataset.data_set.load_by_id(id)
print("Captured run id: {}".format(dataset.captured_run_id))
print("Sample name: {}".format(dataset.sample_name))
print("Experiment name: {}".format(dataset.exp_name))
print(dataset.get_parameter_data)
t_general=dataset.get_parameter_data("Time").get("Time").get("Time").copy()
v_ec = dataset.get_parameter_data("keithley1_source_voltage").get("keithley1_source_voltage").get("keithley1_source_voltage").copy()
v_bc = dataset.get_parameter_data("keithley2_source_voltage").get("keithley2_source_voltage").get("keithley2_source_voltage").copy()
i_e = dataset.get_parameter_data("keithley1_sense_current").get("keithley1_sense_current").get("keithley1_sense_current").copy()
i_b = dataset.get_parameter_data("keithley2_sense_current").get("keithley2_sense_current").get("keithley2_sense_current").copy()
i_c=((-1*i_e)-i_b)
v_be=v_bc-v_ec
t=t_general[0::4]
# Beta_name="keithley2_sense_current"
# try: t=dataset.get_parameter_data(Beta_name).get(Beta_name).get("Time")
#%%I_B(t)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel("Elapsed time (s)", size=size)
ax.set_ylabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y=i_b*1e9
v1=str(v_ec[0]*1e3)+" "
v2=str(v_bc[0]*1e3)+" "
ax.scatter(x, y,marker=".", s=point_size, label=r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+" (mV)")
#ax.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize-2})
ax.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=labelsize)
ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")

L1=i_b[i_b>0.645e-9]
L2=i_b[np.logical_and(i_b<=0.645e-9, i_b>=0.642e-9)]
L3=i_b[i_b<0.642e-9]
L1=np.mean(L1)*1e9
L2=np.mean(L2)*1e9
L3=np.mean(L3)*1e9
dist1=L1-L2
dist2=L2-L3
plt.savefig("I_B(t)_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_B(t)_id={0:s}.pdf".format(id), bbox_inches='tight') 
#%%I_E(t)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel("Elapsed time (s)", size=size)
ax.set_ylabel(r"$I_\mathrm{E}$ (nA)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y=i_e*1e9
v1=str(v_ec[0]*1e3)+" "
v2=str(v_bc[0]*1e3)+" "
ax.scatter(x, y,marker=".",s=point_size, label=r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+"(mV)")
#ax.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize-2})
ax.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=labelsize)
ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
plt.savefig("I_E(t)_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_E(t)_id={0:s}.pdf".format(id), bbox_inches='tight') 
#%% 𝐼_𝐸(𝐼_𝐵)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"$|I_\mathrm{E}|$ (nA)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=i_b*1e9
y=abs(i_e*1e9)
v1=str(v_ec[0]*1e3)+" "
v2=str(v_bc[0]*1e3)+" "
ax.scatter(x, y,marker=".", s=point_size, label=r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+"(mV)")
#ax.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize-2})
ax.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=labelsize)
ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
plt.savefig("I_E(I_B)_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_E(I_B)_id={0:s}.pdf".format(id), bbox_inches='tight') 
#%% 𝐼_𝐵 histogram
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
plt.xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
plt.ylabel("Counts", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
# ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=i_b*1e9
# y=Counter(x)
v1=str(v_ec[0]*1e3)+" "
v2=str(v_bc[0]*1e3)+" "
# s = pd.Series(y,index="values")
# s=pd.Series.swapaxes(s)
sns.histplot()
# df = pd.DataFrame(y.keys())
sns.histplot(x,color="green",kde=True ,label=r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+"(mV)") # discrete=True
#ax.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize-2})
ax.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=labelsize)
ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
plt.savefig("I_B_hist_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_B_hist_id={0:s}.pdf".format(id), bbox_inches='tight') 
#%% I_E histogram
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
plt.xlabel(r"$I_\mathrm{E}$ (nA)", size=size)
plt.ylabel("Counts", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
# ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=i_e*1e9
# y=Counter(x)
v1=str(v_ec[0]*1e3)+" "
v2=str(v_bc[0]*1e3)+" "
# s = pd.Series(y,index="values")
# s=pd.Series.swapaxes(s)
# df = pd.DataFrame(y.keys())
sns.histplot(x,color="green",kde=True, label=r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+"(mV)") # discrete=True
#ax.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize-2})
ax.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=labelsize)
ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
plt.savefig("I_E_hist_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_E_hist_id={0:s}.pdf".format(id), bbox_inches='tight')
#%% I_B(t) and I_E(t) Twin Axes plot
fig,ax1 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax1.set_xlabel("Elapsed time (s)", size=size)
ax1.set_ylabel(r"$I_\mathrm{B}$ (nA)", size=size, color='blue')
ax1.grid()
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y1=i_b*1e9
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
ax1.scatter(x, y1, marker=".", s=point_size, color='blue')
ax1.tick_params(labelsize=labelsize)
ax2=ax1.twinx()
ax2.set_ylabel(r"$I_\mathrm{E}$ (nA)", size=size, color='red')
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.tick_params(labelsize=labelsize)
y2=i_e*1e9
ax2.scatter(x, y2,marker=".", s=point_size, color='red')
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
ax1.tick_params(width=tick_width, length=7, which="major",direction="in")
ax1.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax2.tick_params(width=tick_width, length=7, which="major",direction="in")
ax2.tick_params(width=tick_width, length=4, which="minor",direction="in")
for label in ax1.get_yticklabels():label.set_color("blue")
for label in ax2.get_yticklabels():label.set_color("red")
ax1.set_title(r"$V_\mathrm{EC}$="+v1+r" (mV), $V_\mathrm{BC}$="+v2+" (mV)", fontsize=size)
plt.savefig("I_E_B_twin_axes_id={0:s}.png".format(id), bbox_inches='tight')
plt.savefig("I_E_B_twin_axes_id={0:s}.pdf".format(id), bbox_inches='tight')
