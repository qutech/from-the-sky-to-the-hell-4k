# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 18:27:54 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#%matplotlib auto
#import seaborn as sns
import math

#%% design global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 - 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
n_points=51
size=11
labelsize=8
point_size=1
tick_width=0.5
#%%
p1 = "C:/Users/levon/Desktop/DATA/HBT/416L-L2-D3/4K with IV converter- Saturation mode measurments/11-06-2021/#009_12-27-47_2D_Sweep"
p2 = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D2/4K with IV converter- Saturation mode measurments/17-06-2021/#003_20-42-01_2D_Sweep"
p3 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with IV converter- Saturation mode measurments/03-06-2021/#033_15-52-52_2D_Sweep"
p4 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D4/4K with IV converter- Saturation mode measurments/03-06-2021/#012_17-52-17_2D_Sweep"


os.chdir("C:/Users/levon/Desktop/DATA/HBT/416L-Ls_S_comaprison")

fig1,ax1 = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax1.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax1.set_ylabel(r"$\beta$", rotation=0, size=size)
ax1.grid()
# ax1.set_yscale('log')
fig2,ax2 = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax2.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax2.set_ylabel(r"AC $\beta$", size=size)
ax2.grid()
fig3,ax3 = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax3.set_xlabel(r"$V_\mathrm{BE}$ (mV)", size=size)
ax3.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax3.grid()
# ax3.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax3.yaxis.set_major_formatter(formatter)
ax3.set_yscale('log')
fig4,ax4 = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax4.set_xlabel(r"$P$ (nW)", size=size)
ax4.set_ylabel(r"$g_\mathrm{m} $ (mS)", size=size)
ax4.yaxis.set_major_formatter(formatter)
ax4.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.grid()
fig5,ax5 = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax5.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax5.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax5.grid()
ax5.yaxis.set_major_formatter(formatter)
ax5.set_yscale('log')
p=[p1, p2, p3, p4]
safe= ["#88CCEE","#CC6677","#DDCC77","#117733","#332288","#AA4499","#44AA99","#999933","#882255","#661100","#6699CC","#888888"]
ax1.set_prop_cycle(color=safe)
ax2.set_prop_cycle(color=safe)
ax3.set_prop_cycle(color=safe)
ax4.set_prop_cycle(color=safe)
ax5.set_prop_cycle(color=safe)
n=1
for k in p:
    dataset=qc.data.data_set.load_data(k)
    print(dataset.read)
    g1=dataset.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
    g2=dataset.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
    v_ec = dataset.qdac_chan01_v.ndarray.copy()
    v_bc = dataset.qdac_chan02_v.ndarray.copy()
    i_c = dataset.dmm1_volt.ndarray.copy()*1/g1*-1
    i_b = dataset.dmm2_volt.ndarray.copy()*1/g2*-1
    i_e=i_c+i_b

    i_e[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
    i_b[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
    # i_b[i_e==np.nanmin(i_e)]=np.nan
    # i_e[i_e==np.nanmin(i_e)]=np.nan
    mask=np.isnan(i_b)

    v_ec[mask]=np.nan
    v_bc[mask]=np.nan
    i_b[mask]=np.nan

    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


    Beta=i_c/i_b
    # if np.nanmax(Beta)>2000:
    #     v_be[Beta==np.nanmax(Beta)]=np.nan
    #     Beta[Beta==np.nanmax(Beta)]=np.nan
    X1=i_b*1e9
    # indexs_to_order_by = X1.argsort()
    # x_ordered = X1[indexs_to_order_by]
    # y_ordered = Beta[indexs_to_order_by]
    # mask1=y_ordered<0
    # y_ordered[mask1]=np.nan
    Sampname=k[32:42]
    ax1.scatter(X1, Beta,marker=".", s=point_size, label=Sampname)
    AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
    AC_Beta[AC_Beta<0]=np.nan
    AC_Beta[AC_Beta>750]=np.nan
    # if np.nanmax(AC_Beta)>2000:
    #     AC_Beta[AC_Beta==np.nanmax(AC_Beta)]=np.nan
    X2=X1
    ax2.scatter(X2, AC_Beta, marker=".", s=point_size, label=Sampname)
    d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
    X3=v_be*1e3
    # Indexs_to_order_by = X.argsort()
    # X_ordered = X[Indexs_to_order_by]
    # Y_ordered = (d𝑔_𝑚)[Indexs_to_order_by]
    mask2=d𝑔_𝑚<0
    d𝑔_𝑚[mask2]=np.nan
    # if np.nanmax(d𝑔_𝑚)>0.3e-3:
    #     d𝑔_𝑚[d𝑔_𝑚==np.nanmax(d𝑔_𝑚)]=np.nan
    ax3.scatter(X3[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", s=point_size, label=Sampname)
    X4=Pwr*1e9
    ax4.scatter(X4[:,1:-2], d𝑔_𝑚[:,1:-2]*1e3, marker=".", s=point_size, label=Sampname)
    X5=i_b*1e9
    ax5.scatter(X5[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", s=point_size, label=Sampname)
    n+=1
leg1=ax1.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize})
for line in leg1.get_lines():
    line.set_linewidth(2)
for handle in leg1.legendHandles:
    handle.set_sizes([120.0])
leg2=ax2.legend(loc=2, borderaxespad=0.0, prop={'size': labelsize})
for line in leg1.get_lines():
    line.set_linewidth(2)
for handle in leg2.legendHandles:
    handle.set_sizes([120.0])
leg3=ax3.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize})
for line in leg3.get_lines():
    line.set_linewidth(2)
for handle in leg3.legendHandles:
    handle.set_sizes([120.0])
leg4=ax4.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize})
for line in leg4.get_lines():
    line.set_linewidth(2)
for handle in leg4.legendHandles:
    handle.set_sizes([120.0])
    leg5=ax5.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize})
for line in leg5.get_lines():
    line.set_linewidth(2)
for handle in leg5.legendHandles:
    handle.set_sizes([120.0])
ax1.tick_params(labelsize=labelsize)
ax2.tick_params(labelsize=labelsize)
ax3.tick_params(labelsize=labelsize)
ax4.tick_params(labelsize=labelsize)
ax5.tick_params(labelsize=labelsize)
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax5.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax5.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
  ax2.spines[axis].set_linewidth(2)
  ax3.spines[axis].set_linewidth(2)
  ax4.spines[axis].set_linewidth(2)
  ax5.spines[axis].set_linewidth(2)
ax1.tick_params(width=tick_width, length=7, which="major",direction="in")
ax1.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax2.tick_params(width=tick_width, length=7, which="major",direction="in")
ax2.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax3.tick_params(width=tick_width, length=7, which="major",direction="in")
ax3.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax4.tick_params(width=tick_width, length=7, which="major",direction="in")
ax4.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax5.tick_params(width=tick_width, length=7, which="major",direction="in")
ax5.tick_params(width=tick_width, length=4, which="minor",direction="in")


ax4.set_xlim(xmin=0, xmax=200)
ax4.set_ylim(ymin=0, ymax=0.3)

fig1.savefig("416L_S_comparison1.pdf", bbox_inches='tight') 
fig1.savefig("416L_S_comparison1.png", bbox_inches='tight')
fig2.savefig("416L_S_comparison2.pdf", bbox_inches='tight') 
fig2.savefig("416L_S_comparison2.png", bbox_inches='tight')
fig3.savefig("416L_S_comparison3.pdf", bbox_inches='tight') 
fig3.savefig("416L_S_comparison3.png", bbox_inches='tight')
fig4.savefig("416L_S_comparison4.pdf", bbox_inches='tight') 
fig4.savefig("416L_S_comparison4.png", bbox_inches='tight')
fig5.savefig("416L_S_comparison5.pdf", bbox_inches='tight') 
fig5.savefig("416L_S_comparison5.png", bbox_inches='tight')


    
