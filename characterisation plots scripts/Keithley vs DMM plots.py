# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 11:36:01 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#%matplotlib auto
#import seaborn as sns
import math

p1 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable/09-02-2021/#004_2D_Sweep_12-50-27/keithley1_source_voltage_set_keithley2_source_voltage_set"
p2 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable/22-03-2021/#002_16-35-21_2D_Sweep/dac_Slot0_Chan0_volt_set_dac_Slot0_Chan1_volt_set.dat"
p3 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable TTs with Keithleys/27-03-2021/#001_16-59-26_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p4 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable/27-03-2021/#010_11-35-15_2D_Sweep/dac_Slot0_Chan0_volt_set_dac_Slot0_Chan1_volt_set.dat"
fig1,ax1 = plt.subplots(figsize=(12,12))
ax1.set_xlabel(r"$I_B$ (nA)", size=20)
ax1.set_ylabel(r"$\beta$", rotation=0, size=20)
ax1.grid()
# ax.set_yscale('log')
fig2,ax2 = plt.subplots(figsize=(12,12))
ax2.set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax2.set_ylabel(r"$g_m$ (S)", size=20)
ax2.grid()
# ax2.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax2.yaxis.set_major_formatter(formatter)
ax2.set_yscale('log')
fig3,ax3 = plt.subplots(figsize=(12,12))
ax3.set_xlabel(r"$P$ (nW)", size=20)
ax3.set_ylabel(r"$g_m$ (mS)", size=20)
ax3.yaxis.set_major_formatter(formatter)
ax3.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.grid()
fig4,ax4 = plt.subplots(figsize=(12,12))
ax4.set_xlabel(r"$I_B$ (nA)", size=20)
ax4.set_ylabel(r"$g_m$ (S)", size=20)
ax4.grid()
ax4.yaxis.set_major_formatter(formatter)
ax4.set_yscale('log')
p=[p1, p2, p3, p4]
n=0
name=["Keithley 2450 cooldown 1", "DecaDAC and Keysight 34461A cooldown 2", "Keithley 2450 cooldown 3", "DecaDAC and Keysight 34461A cooldown 3"]
for k in p:
    dataset=qc.data.data_set.load_data(k)
    print(dataset.read)
    if k==p2 or k==p4:
        v_ec = dataset.dac_Slot0_Chan0_volt.ndarray.copy()
        v_bc = dataset.dac_Slot0_Chan1_volt.ndarray.copy()/100
        i_c = dataset.dmm1_volt.ndarray.copy()*1e-7*-1
        i_b = dataset.dmm2_volt.ndarray.copy()*1e-7*-1
        i_e=i_c+i_b
    else:
        v_ec = dataset.keithley1_source_voltage.ndarray.copy()
        v_bc = dataset.keithley2_source_voltage.ndarray.copy()
        i_e = dataset.keithley1_sense_current.ndarray.copy()
        i_b = dataset.keithley2_sense_current.ndarray.copy()
        i_c=((-1*i_e)-i_b)
        
        v_ec[np.logical_or(v_ec<-943e-3, v_ec>-902e-3)]=np.nan
        mask=np.isnan(v_ec)
        v_ec[mask]=np.nan
        v_bc[mask]=np.nan
        i_b[mask]=np.nan
        i_c[mask]=np.nan
        
    v_be= v_bc-v_ec 
    Pwr=i_c*v_ec*-1+i_b*v_be
    Pwr[Pwr<0]=np.nan
    i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    mask=np.isnan(i_b)
    v_ec[mask]=np.nan
    v_bc[mask]=np.nan
    v_be[mask]=np.nan
    i_b[mask]=np.nan
    i_c[mask]=np.nan

    Beta=i_c/i_b
    if np.nanmax(Beta)>2000:
        v_be[Beta==np.nanmax(Beta)]=np.nan
        Beta[Beta==np.nanmax(Beta)]=np.nan
    x=i_b*1e9
    # indexs_to_order_by = x.argsort()
    # x_ordered = x[indexs_to_order_by]
    # y_ordered = Beta[indexs_to_order_by]
    # mask1=y_ordered<0
    # y_ordered[mask1]=np.nan
    ax1.scatter(x, Beta,marker=".",lw=1, label=name[n] )
    d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
    X=v_be*1e3
    # Indexs_to_order_by = X.argsort()
    # X_ordered = X[Indexs_to_order_by]
    # Y_ordered = (d𝑔_𝑚)[Indexs_to_order_by]
    mask2=d𝑔_𝑚<0
    d𝑔_𝑚[mask2]=np.nan
    ax2.scatter(X[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", lw=1, label=name[n])
    XX=Pwr*1e9
    ax3.scatter(XX[:,1:-2], d𝑔_𝑚[:,1:-2]*1e3, marker=".", lw=1, label=name[n])
    X4=i_b*1e9
    ax4.scatter(X4[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", lw=1, label=name[n])
    n+=1
ax1.legend(loc=4, borderaxespad=0.0)
ax2.legend(loc=4, borderaxespad=0.0)
ax3.legend(loc=4, borderaxespad=0.0)
ax4.legend(loc=4, borderaxespad=0.0)
ax1.tick_params(labelsize=16)
ax2.tick_params(labelsize=16)
ax3.tick_params(labelsize=16)
ax4.tick_params(labelsize=16)
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
  ax2.spines[axis].set_linewidth(2)
  ax3.spines[axis].set_linewidth(2)
  ax4.spines[axis].set_linewidth(2)
ax1.tick_params(width=1.5, length=12, which="major",direction="in")
ax1.tick_params(width=1.5, length=6, which="minor",direction="in")
ax2.tick_params(width=1.5, length=12, which="major",direction="in")
ax2.tick_params(width=1.5, length=6, which="minor",direction="in")
ax3.tick_params(width=1.5, length=12, which="major",direction="in")
ax3.tick_params(width=1.5, length=6, which="minor",direction="in")
ax4.tick_params(width=1.5, length=12, which="major",direction="in")
ax4.tick_params(width=1.5, length=6, which="minor",direction="in")
