# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 14:55:12 2021

@author: levon
"""



import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#
import seaborn as sns
import math
from pprint import pprint
 

p="C:/Users/levon/Desktop/DATA/HBT/416L-L2-D3/4K with grounding cable/08-02-2021/#008_15-42-10_2D_Sweep"


dataset=qc.data.data_set.load_data(p)
print(dataset.read)
v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_source_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()*-1
i_b = dataset.keithley2_sense_current.ndarray.copy()

i_c=i_e-i_b
i_e[np.logical_or(i_b>5e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>5e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
# Pwr[np.where(Pwr>100*1e-9)]=np.nan

p2 = "C:/Users/levon/Desktop/DATA/HBT/416L-L2-D3/4K with IV converter- Saturation mode measurments/11-06-2021/#009_12-27-47_2D_Sweep"
dataset2=qc.data.data_set.load_data(p2)
print(dataset2.read)
g1=dataset2.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset2.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec2 = dataset2.qdac_chan01_v.ndarray.copy()
v_bc2 = dataset2.qdac_chan02_v.ndarray.copy()
i_c2 = dataset2.dmm1_volt.ndarray.copy()*1/g1*-1
i_b2 = dataset2.dmm2_volt.ndarray.copy()*1/g2*-1
i_e2=i_c2+i_b2
i_e2[np.logical_or(i_b2>5e-9, i_b2<0)]=np.nan
i_b2[np.logical_or(i_b2>5e-9, i_b2<0)]=np.nan
# i_b2[i_e==np.nanmin(i_e)]=np.nan
# i_e2[i_e==np.nanmin(i_e)]=np.nan
mask2=np.isnan(i_b2)
v_ec2[mask]=np.nan
v_bc2[mask]=np.nan
i_b2[mask]=np.nan
v_be2=v_bc2-v_ec2
Pwr2=np.abs(i_c2*v_ec2)+np.abs(i_b2*v_be2)
#%% design global parameters
cb_colour="viridis_r"
n_points=51
cb_colour2="inferno"
#%% 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0,  size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
Beta2=i_c2/i_b2
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.06)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=i_b2[n,:]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = Beta2[n,:][indexs_to_order_by2]
    mask2=y_ordered<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".",lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.06)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
# ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% diff 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"AC $\beta$", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta2=np.gradient(i_c2,axis=1)/np.gradient(i_b2,axis=1)
AC_Beta[AC_Beta>300]=np.nan
AC_Beta2[AC_Beta2>300]=np.nan
while n<= (n_points-1):
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".",lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.09)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=i_b2[n,1:-2]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = AC_Beta2[n,1:-2][indexs_to_order_by2]
    mask2=y_ordered2<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".",lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.09)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% diff 𝛽(𝐼_𝐵) log scale
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"AC $\beta$", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta2=np.gradient(i_c2,axis=1)/np.gradient(i_b2,axis=1)
while n<= (n_points-1):
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".",lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.09)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=i_b2[n,1:-2]*1e9
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    y_ordered2 = AC_Beta2[n,1:-2][indexs_to_order_by2]
    mask2=y_ordered2<0
    y_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".",lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.09)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(V_BE)
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n<= (n_points-1):
    x=v_be[n,1:-2]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.09)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=v_be2[n,1:-2]*1e3
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.09)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$g_m $ (mS)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n<= (n_points-1):
    x=Pwr[n,1:-2]*1e9
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]*1e03
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.09)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=Pwr2[n,1:-2]*1e9
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]*1e03
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.09)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")


#%% 𝑔_𝑚(i_b)
fig,ax = plt.subplots(figsize=(16,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
ax.tick_params(labelsize=16)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚2=np.gradient(i_c2,axis=1)/np.gradient(v_be2,axis=1)
while n<= (n_points-1):
    x=i_b[n,1:-2]*1e9
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered, marker=".", lw=1)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(-v_ec)*1e3, vmax=np.nanmax(-v_ec)*1e3))
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
cb=plt.colorbar(sm,ax=[ax],location='left', pad = 0.09)
cb.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
cb.ax.set_xlabel(r"Forward regime", size=14)

cm2 = plt.get_cmap(cb_colour2)
ax.set_prop_cycle(color=[cm2(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
n=0
while n<= (n_points-1):
    x2=i_b2[n,1:-2]*1e9
    X2=x2
    indexs_to_order_by2 = X2.argsort()
    x_ordered2 = X2[indexs_to_order_by2]
    y_ordered2 = (d𝑔_𝑚2[n,1:-2])[indexs_to_order_by2]
    mask2=y_ordered2<0
    x_ordered2[mask2]=np.nan
    ax.scatter(x_ordered2, y_ordered2, marker=".", lw=1)
    n+=1
sm2 = plt.cm.ScalarMappable(cmap=cm2, norm=plt.Normalize(vmin=np.nanmin(-v_ec2)*1e3, vmax=np.nanmax(-v_ec2)*1e3))
cb2=plt.colorbar(sm2, ax=[ax],location='right', pad = 0.09)
cb2.ax.set_title(r"$V_{CE}$ (mV)", size=20)
cb2.ax.tick_params(labelsize=12)
cb2.ax.set_xlabel(r"Saturation regime", size=14)
ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")