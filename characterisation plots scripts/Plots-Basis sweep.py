# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 15:37:05 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#%matplotlib auto
#import seaborn as sns
import math
 

p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D4/4K with grounding cable/18-02-2021/#008_18-22-39_Basis Sweep/keithley2_source_voltage_set"

dataset=qc.data.data_set.load_data(p)
print(dataset.read)
#dataset.default_parameter_name()
#dataset.default_parameter_array()
#dataset.read
# A=dataset.default_parameter_array()
# J=dataset.keithley1_sense_current.ndarray
# C=A.ndarray
v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_source_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()
i_b = dataset.keithley2_sense_current.ndarray.copy()

# i_e[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
# i_b[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
# mask=np.isnan(i_b)

v_be=v_bc-v_ec
i_c=((-1*i_e)-i_b) #or directly measure it
Pwr=i_c*v_ec*-1+i_b*v_be
# Pwr[Pwr<0]=np.nan

# v_ec[mask]=np.nan
# v_bc[mask]=np.nan
# v_be[mask]=np.nan
# i_c[mask]=np.nan
# i_b[mask]=np.nan
# i_c[mask]=np.nan


#%% 𝐼_𝐸(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$|I_E|$ (nA)", size=20)
ax.grid()
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
x=i_b*1e9
y=abs(i_e*1e9)
#Order points by their x-value
ax.scatter(x, y,marker=".",lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#%% 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
Beta=i_c/i_b
x=i_b*1e9
indexs_to_order_by = x.argsort()
# mask=y_ordered<0
# y_ordered[mask]=np.nan
ax.scatter(x, Beta,marker=".",lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_yscale('log')
#%% 𝛽(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
Beta=i_c/i_b
x=Pwr*1e9
indexs_to_order_by = x.argsort()
# mask=y_ordered<0
# y_ordered[mask]=np.nan
ax.scatter(x, Beta,marker=".",lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_yscale('log')
#%% 𝑔_𝑚(V_BE)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
offset= 0 #(mS)
d𝑔_𝑚=np.ones(999) 
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=v_be*1e3
mask=d𝑔_𝑚<0
d𝑔_𝑚[mask]=np.nan
ax.scatter(x, d𝑔_𝑚+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# hmm=plt.ginput(50)
#%% 𝑟_𝜋(𝐼_𝐵)
fig,ax = plt.subplots(1, 1, figsize=(12,12),)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax.grid()
r_pi=np.gradient(v_be)/np.gradient(i_b)
x=i_b*1e9
mask=r_pi<0
r_pi[mask]=np.nan
ax.plot(x, r_pi, marker=".", lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=Pwr*1e9
mask=d𝑔_𝑚<0
d𝑔_𝑚[mask]=np.nan
ax.scatter(x, d𝑔_𝑚+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# hmm=plt.ginput(50)
#%% 𝑟_𝜋(Pwr)
fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax.grid()
r_pi=np.gradient(v_be)/np.gradient(i_b)
x=Pwr*1e9
mask=r_pi<0
r_pi[mask]=np.nan
ax.plot(x, r_pi, marker=".", lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#%% 𝑔_𝑚(i_b)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=i_b*1e9
mask=d𝑔_𝑚<0
d𝑔_𝑚[mask]=np.nan
ax.scatter(x, d𝑔_𝑚+offset, marker=".", lw=1, label=r"$V_{EC}$=%.1f $(mV)$" % -920)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())