# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 09:36:35 2021

@author: levon
"""
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#
import seaborn as sns
import math
from pprint import pprint
 
p = "C:/Users/levon/Desktop/DATA/HBT/3-F5/4K with current sourcing- Saturation mode measurments/31-05-2021/#003_18-44-05_Basis Sweep with current sourcing/keithley2_source_current_set"


dataset=qc.data.data_set.load_data(p)
print(dataset.read)
v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_sense_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()
i_b = dataset.keithley2_source_current.ndarray.copy()

i_c=i_e*-1-i_b
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_c[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_e[mask]=np.nan

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


# Pwr[np.where(Pwr>20000*1e-9)]=np.nan
# Beta[Beta==np.nanmax(Beta)]=np.nan

# mask=np.logical_and(i_b>=0.1e-9, i_b<=10e-9)
# A1=v_ec[mask]
# B1=v_bc[mask]
# A2=v_ec[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=v_be[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=np.sort(B2)
#%% 𝐼_C,𝐼_𝐵 as a function of V_BC
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$V_{BC}$ (mV)", size=20)
ax.set_ylabel(r"Current (nA)", size=20)
ax.grid()
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
x=v_bc*1e3
y1=i_c*1e9
y2=i_b*1e9
v=np.around(v_ec[0]*1e3)
#Order points by their x-value
ax.scatter(x, y1,marker=".",lw=1, color="maroon", label=r"$I_C$ (nA) at $V_{EC}$=%.0f $(mV)$" % v)
ax.scatter(x, y2,marker=".",lw=1, color="navy", label=r"$I_B$ (nA) at $V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#%% 𝐼_C(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$I_C$ (nA)", size=20)
ax.grid()
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
x=i_b*1e9
y=i_c*1e9
v=np.around(v_ec[0]*1e3)
#Order points by their x-value
ax.scatter(x, y,marker=".", color="navy",lw=1, label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#%% 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
Beta=i_c/i_b
x=i_b*1e9
indexs_to_order_by = x.argsort()
# mask=y_ordered<0
# y_ordered[mask]=np.nan
v=np.around(v_ec[0]*1e3)
ax.scatter(x, Beta, color="navy",marker=".",lw=1, label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% diff 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"AC $\beta$", size=20)
ax.grid()
AC_Beta=np.gradient(i_c)/np.gradient(i_b)
x=i_b*1e9
v=np.around(v_ec[0]*1e3)
mask=AC_Beta<0
x[mask]=np.nan
ax.scatter(x[1:-2], AC_Beta[1:-2],marker=".",lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")

#%% 𝛽(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
Beta=i_c/i_b
x=Pwr*1e9
indexs_to_order_by = x.argsort()
# mask=y_ordered<0
# y_ordered[mask]=np.nan
v=np.around(v_ec[0]*1e3)
ax.scatter(x, Beta,marker=".",lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(V_BE)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=v_be*1e3
mask=d𝑔_𝑚<0
d𝑔_𝑚[mask]=np.nan
v=np.around(v_ec[0]*1e3)
ax.scatter(x[1:-2], d𝑔_𝑚[1:-2], marker=".", lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑟_𝜋(𝐼_𝐵)
fig,ax = plt.subplots(1, 1, figsize=(12,12),)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax.grid()
r_pi=np.gradient(v_be)/np.gradient(i_b)
x=i_b*1e9
mask=r_pi<0
r_pi[mask]=np.nan
v=np.around(v_ec[0]*1e3)
ax.scatter(x[1:-2], r_pi[1:-2], marker=".", lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=Pwr*1e9
mask=d𝑔_𝑚<0
v=np.around(v_ec[0]*1e3)
d𝑔_𝑚[mask]=np.nan
ax.scatter(x[1:-2], d𝑔_𝑚[1:-2], marker=".", lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# hmm=plt.ginput(50)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑟_𝜋(Pwr)
fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax.grid()
r_pi=np.gradient(v_be)/np.gradient(i_b)
x=Pwr*1e9
v=np.around(v_ec[0]*1e3)
mask=r_pi<0
r_pi[mask]=np.nan
ax.scatter(x[1:-2], r_pi[1:-2], marker=".", lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(i_b)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
d𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
x=i_b*1e9
mask=d𝑔_𝑚<0
d𝑔_𝑚[mask]=np.nan
v=np.around(v_ec[0]*1e3)
ax.scatter(x[1:-2], d𝑔_𝑚[1:-2], marker=".", lw=1, color="navy", label=r"$V_{EC}$=%.0f $(mV)$" % v)
ax.legend(loc=4, borderaxespad=0.0)
ax.set_yscale('log')
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")