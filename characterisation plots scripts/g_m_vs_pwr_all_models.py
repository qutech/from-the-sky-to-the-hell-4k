# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 10:47:51 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#
import seaborn as sns
import math
from pprint import pprint
 
#p = "C:/Users/Baghumyan/Desktop/L6-D2/Qdevil2"
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D2/4K with IV converter- Saturation mode measurments/17-06-2021/#003_20-42-01_2D_Sweep"
os.chdir("C:/Users/levon/Desktop/S_final/conclusion_plots")
Sampname=p[32:42]

dataset=qc.data.data_set.load_data(p)
print(dataset.read)
#dataset.default_parameter_name()
#dataset.default_parameter_array()
#dataset.read
# A=dataset.default_parameter_array()
# J=dataset.keithley1_sense_current.ndarray
# C=A.ndarray
g1=dataset.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec = dataset.qdac_chan01_v.ndarray.copy()
v_bc = dataset.qdac_chan02_v.ndarray.copy()
i_c = dataset.dmm1_volt.ndarray.copy()*1/g1*-1
i_b = dataset.dmm2_volt.ndarray.copy()*1/g2*-1
i_e=i_c+i_b

i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


# Pwr[np.where(Pwr>20000*1e-9)]=np.nan
# R[Beta==np.nanmax(Beta)]=np.nan

# mask=np.logical_and(i_b>=0.1e-9, i_b<=10e-9)
# A1=v_ec[mask]
# B1=v_bc[mask]
# A2=v_ec[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=v_be[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=np.sort(B2)

#%% design global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
cb_colour="inferno"
n_points=51
size=11
labelsize=8
point_size=1
tick_width=0.5

#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax.set_xlabel(r"$P$ (nW)", size=size)
ax.set_ylabel(r"$g_\mathrm{m} $ (mS)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
safe= ["#88CCEE","#CC6677","#DDCC77","#888888"]
ax.set_prop_cycle(color=safe)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
x=Pwr*1e9
X=x
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = (d𝑔_𝑚)[indexs_to_order_BY]*1e03
mask=Y_ordered<0
Y_ordered[mask]=np.nan
ax.scatter(X_ordered, Y_ordered, marker=".", s=point_size, label=Sampname)
# ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
#%% B1
p = "C:/Users/levon/Desktop/DATA/HBT/1-B1/4K with IV converter- Saturation mode measurments/26-06-2021/#005_16-07-52_2D_Sweep"
Sampname="BFP842ESD, "+p[32:36]
dataset=qc.data.data_set.load_data(p)
print(dataset.read)
g1=dataset.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec = dataset.qdac_chan01_v.ndarray.copy()
v_bc = dataset.qdac_chan02_v.ndarray.copy()
i_c = dataset.dmm1_volt.ndarray.copy()*1/g1*-1
i_b = dataset.dmm2_volt.ndarray.copy()*1/g2*-1
i_e=i_c+i_b
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
x=Pwr*1e9
X=x
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = (d𝑔_𝑚)[indexs_to_order_BY]*1e03
mask=Y_ordered<0
Y_ordered[mask]=np.nan
ax.scatter(X_ordered, Y_ordered, marker=".", s=point_size, label=Sampname)
#%% C1
p = "C:/Users/levon/Desktop/DATA/HBT/1-C1/4K with IV converter- Saturation mode measurments/26-06-2021/#001_16-35-04_2D_Sweep"
Sampname="BFP840ESD, "+p[32:36]
dataset=qc.data.data_set.load_data(p)
print(dataset.read)
g1=dataset.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec = dataset.qdac_chan01_v.ndarray.copy()
v_bc = dataset.qdac_chan02_v.ndarray.copy()
i_c = dataset.dmm1_volt.ndarray.copy()*1/g1*-1
i_b = dataset.dmm2_volt.ndarray.copy()*1/g2*-1
i_e=i_c+i_b
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
x=Pwr*1e9
X=x
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = (d𝑔_𝑚)[indexs_to_order_BY]*1e03
mask=Y_ordered<0
Y_ordered[mask]=np.nan
ax.scatter(X_ordered, Y_ordered, marker=".", s=point_size, label=Sampname)
#%% F5
p = "C:/Users/levon/Desktop/DATA/HBT/3-F5/4K with current sourcing- Saturation mode measurments/31-05-2021/#002_17-59-33_2D_Sweep with current sourcing/keithley1_source_voltage_set_keithley2_source_current_set"
Sampname="BFP720ESD, "+p[32:36]
dataset=qc.data.data_set.load_data(p)
print(dataset.read)
v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_sense_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()
i_b = dataset.keithley2_source_current.ndarray.copy()
i_c=i_e*-1-i_b
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
Pwr[Pwr<0]=np.nan
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
d𝑔_𝑚[d𝑔_𝑚>2e-4]=np.nan
x=Pwr*1e9
v=v_ec[:,0]*1e3
X=x
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = (d𝑔_𝑚)[indexs_to_order_BY]*1e03
mask=Y_ordered<0
Y_ordered[mask]=np.nan
ax.scatter(X_ordered, Y_ordered, marker=".", s=point_size, label=Sampname)
#%%saving
ax.set_xlim(xmin=0,xmax=200)
ax.set_ylim(ymin=0,ymax=0.4)
ax.tick_params(labelsize=labelsize)
leg=ax.legend(loc=2, borderaxespad=0.0, prop={'size': labelsize})
# for line in leg.get_lines():
#     line.set_linewidth(2)
for handle in leg.legendHandles:
    handle.set_sizes([120.0])
plt.savefig("Final_comparison_g_m(Pwr).pdf", bbox_inches='tight') 
plt.savefig("Final_comparison_g_m(Pwr).png", bbox_inches='tight')