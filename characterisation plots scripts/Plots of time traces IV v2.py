# -*- coding: utf-8 -*-
"""
Created on Thu May 27 18:15:26 2021

@author: levon
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 11:48:54 2021

@author: Baghumyan
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from collections import Counter
#%% Traditional QCoDeS way
p = "C:/Users/levon/Desktop/DATA/HBT/2-F1/4K with IV converter-Saturation mode measurments and TTs/27-05-2021/27-05-2021 at 11_51_06 o_clock.db"
initialise_or_create_database_at(p)
from qcodes.dataset.experiment_container import experiments
print(experiments())
time.sleep(0.5)
id=input("Please enter the experiment run id that you wish to use for calculations \nid=")
dataset=qc.dataset.data_set.load_by_id(id)
print("Captured run id: {}".format(dataset.captured_run_id))
print("Sample name: {}".format(dataset.sample_name))
print("Experiment name: {}".format(dataset.exp_name))
print(dataset.get_parameter_data)
g1=dataset.snapshot.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset.snapshot.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
t_general=dataset.get_parameter_data("Time").get("Time").get("Time").copy()
v_ec = dataset.get_parameter_data("dac_Slot0_Chan0_volt").get("dac_Slot0_Chan0_volt").get("dac_Slot0_Chan0_volt").copy()
v_bc = dataset.get_parameter_data("dac_Slot0_Chan1_volt").get("dac_Slot0_Chan1_volt").get("dac_Slot0_Chan1_volt").copy()
i_c = dataset.get_parameter_data("dmm1_volt").get("dmm1_volt").get("dmm1_volt").copy()*1/g1*-1
i_b = dataset.get_parameter_data("dmm2_volt").get("dmm2_volt").get("dmm2_volt").copy()*1/g2*-1
i_e=i_c+i_b
v_be=v_bc-v_ec
t=t_general[0::4]
# Beta_name="keithley2_sense_current"
# try: t=dataset.get_parameter_data(Beta_name).get(Beta_name).get("Time")
#%%I_B(t)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel("Elapsed time (s)", size=20)
ax.set_ylabel(r"$I_B$ (nA)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y=i_b*1e9
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
ax.scatter(x, y,marker=".",lw=1,label=r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)")
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")

L1=i_b[i_b>0.761e-9]
L2=i_b[np.logical_and(i_b<=0.761e-9, i_b>=0.757e-9)]
L3=i_b[i_b<0.757e-9]
L1=np.mean(L1)*1e9
L2=np.mean(L2)*1e9
L3=np.mean(L3)*1e9
dist1=L1-L2
dist2=L2-L3
#%%I_C(t)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"Elapsed time (s)", size=20)
ax.set_ylabel(r"$I_C$ (nA)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y=i_c*1e9
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
ax.scatter(x, y,marker=".",lw=1,label=r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+"(mV)")
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝐼_C(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$I_C$ (nA)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=i_b*1e9
y=i_c*1e9
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
ax.scatter(x, y,marker=".",lw=1, label=r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+"(mV)")
ax.legend(loc=4, borderaxespad=0.0)
ax.tick_params(labelsize=16)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝐼_𝐵 histogram
# sns.set_style("darkgrid")
fig,ax = plt.subplots(figsize=(12,12))
plt.xlabel(r"$I_B$ (nA)", size=20)
plt.ylabel("Counts", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
x=i_b*1e9
# y=Counter(x)
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
# s = pd.Series(y,index="values")
# s=pd.Series.swapaxes(s)
# sns.histplot()
# df = pd.DataFrame(y.keys())
sns.histplot(x,color="green",label=r"$V_{EC}$="+v1+r" (mV), $V_{BC}$="+v2+"(mV)", kde=True ) # discrete=True
ax.legend(loc=1, borderaxespad=0.0)
ax.tick_params(labelsize=16)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝐼_C histogram
# sns.set_style("darkgrid")
fig,ax = plt.subplots(figsize=(12,12))
plt.xlabel(r"$I_C$ (nA)", size=20)
plt.ylabel("Counts", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid()
x=i_c*1e9
# y=Counter(x)
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
# s = pd.Series(y,index="values")
# s=pd.Series.swapaxes(s)
# df = pd.DataFrame(x)
sns.histplot(x,color="green", label=r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+"(mV)", kde=True )# discrete=True
ax.legend(loc=1, borderaxespad=0.0)
ax.tick_params(labelsize=16)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% I_B(t) and I_C(t) Twin Axes plot
fig,ax1 = plt.subplots(figsize=(12,12))
ax1.set_xlabel("Elapsed time (s)", size=20)
ax1.set_ylabel(r"$I_B$ (nA)", size=20, color='blue')
ax1.grid()
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
x=t
y1=i_b*1e9
v1=str(np.around(v_ec[0]*1e3))+" "
v2=str(np.around(v_bc[0]*1e3))+" "
ax1.scatter(x, y1, marker=".",lw=1, color='blue')
ax1.tick_params(labelsize=16)
ax2=ax1.twinx()
ax2.set_ylabel(r"$I_C$ (nA)", size=20, color='red')
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.tick_params(labelsize=16)
y2=i_c*1e9
ax2.scatter(x, y2,marker=".",lw=1, color='red')
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
ax1.tick_params(width=1.5, length=12, which="major",direction="in")
ax1.tick_params(width=1.5, length=6, which="minor",direction="in")
ax2.tick_params(width=1.5, length=12, which="major",direction="in")
ax2.tick_params(width=1.5, length=6, which="minor",direction="in")
for label in ax1.get_yticklabels():label.set_color("blue")
for label in ax2.get_yticklabels():label.set_color("red")
ax1.set_title(r"$V_{EC}$="+v1+" (mV), $V_{BC}$="+v2+" (mV)", fontsize=20)
