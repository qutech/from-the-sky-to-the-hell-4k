# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 10:41:58 2020

@author: levon
"""

import time
from pathlib import Path
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#from qcodes.instrument_drivers.Harvard.FZJ_Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
#from qcodes.instrument_drivers.agilent.Agilent_34400A import Agilent_34400A
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument

from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot
#matplotlib inline
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

import numpy as np
from tqdm import tqdm

from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
from qcodes.dataset.experiment_container import new_experiment
import numpy as np

from qcodes.instrument.specialized_parameters import ElapsedTimeParameter
t0=time.time()
def experiment_time(t0=t0):
      t= round((time.time()-t0),4)
      return t
  
#%% Load all instruments
qc.Instrument.close_all()
station = qc.Station()
keithley1=Keithley2450('keithley1', 'GPIB1::11::INSTR')
keithley2=Keithley2450('keithley2', 'GPIB1::22::INSTR')
station.add_component(keithley1)
station.add_component(keithley2)
#agilent = Agilent_34400A('agilent', 'GPIB1::28::INSTR')
#lockin=SR830("lockin",'GPIB1::12::INSTR')
#counter=DummyInstrument(name="dummy2")
#counter.add_parameter('count', set_cmd=None)
from qcodes.logger.logger import start_all_logging
#start_all_logging() #a headache, avoid this
#%% Set location here

Expname="4K with current sourcing- Saturation mode measurments"
Sampname="416L-L6-D4"
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D4/4K with current sourcing- Saturation mode measurments/03-06-2021/#001_16-50-06_2D_Sweep with current sourcing"
Path(p).mkdir(parents=True, exist_ok=True)
initialise_or_create_database_at(time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/%d-%m-%Y at %H_%M_%S o'clock.db" ))
exp = load_or_create_experiment(experiment_name=Expname , sample_name=Sampname)

os.chdir(p)

#Additonally for loops
location=time.strftime("Y:/GaAs/Barumjan/Data/HBT/"+Sampname+"/"+Expname+
                                               "/%d-%m-%Y/#{counter}_{time}_{name}")
loc_provider = qc.data.location.FormatLocation(fmt=location)
qc.data.data_set.DataSet.location_provider=loc_provider



#%% Define Values
keithley2.source_function('current')
keithley2.sense_function('voltage')
emitter_device = keithley1
basis_device = keithley2
emitter_channel = emitter_device.source.voltage 
basis_channel = basis_device.source.current

# Emitter Parameters (Voltage)
V_Emitter_Start = -0e-03
V_Emitter_Ziel = -100e-03
npoints_Emitter = 50

# Basis Parameters (Current)
I_Basis_Start = 0.1e-9
I_Basis_Ziel = 4e-09
npoints_Basis = 50

delay_time = 0.1

# lockin_freq = 74.25
# lockin_amp = 0.5

sweep_Emitter_range = [V_Emitter_Start, V_Emitter_Ziel]
sweep_Emitter_step = (sweep_Emitter_range[1] - sweep_Emitter_range[0]) / npoints_Emitter #(npoints_Emitter-1)
sweep_Basis_range = [I_Basis_Start, I_Basis_Ziel]
sweep_Basis_step = (sweep_Basis_range[1] - sweep_Basis_range[0]) / npoints_Basis #(npoints_Basis-1)
#%% Setting measurement speed (in PLC units) and ranges
print(keithley1.line_frequency.get())
print(keithley2.line_frequency.get())
print(keithley1.sense.nplc.get())
print(keithley2.sense.nplc.get())
print(keithley1.source.range.get())
print(keithley2.source.range.get())
nplc=1
keithley1.sense.nplc.set(nplc)
keithley2.sense.nplc.set(nplc)
Range_k1=0.2
Range_k2=10e-09
keithley1.source.range.set(Range_k1)
keithley2.source.range.set(Range_k2)

keithley1.output_enabled(True)
keithley2.output_enabled(True)
# keithley1.print_readable_snapshot()
# keithley2.print_readable_snapshot()
#%%Define ramp_keithley for both parameters
def ramp_keithley_voltage(value, step=0.1, wait=0.01, device=keithley1):
      
      if not device.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device.source_function.get()=='voltage':
            print("Ramping only supported for devices in voltage-source mode. Please set source_funtion to voltage")
            return False
      
      try:  
            if device.source.voltage.get()==value:
                  print("Target value already reached")
                  return True
            assert step > 0
            if device.source.voltage.get()>value:
                  step*=-1                  
            
            set_value=device.source.voltage.get()
            while set_value<value-np.abs(step) or set_value>value+np.abs(step):
                  set_value+=step
                  device.source.voltage.set(set_value)           
                  time.sleep(wait)

            device.source.voltage.set(value)
            
            return True
      except:
            print("Something went wrong")
            
def ramp_keithley_current(value, step=0.1, wait=0.01, device=keithley2):
      
      if not device.output_enabled.get():
            print("Device output is off. Please enable device output first")
            return False
      if not device.source_function.get()=='current':
            print("Ramping only supported for devices in current-source mode. Please set source_funtion to current")
            return False
      
      try:  
            if device.source.current.get()==value:
                  print("Target value already reached")
                  return True
            assert step > 0
            if device.source.current.get()>value:
                  step*=-1                  
            
            set_value=device.source.current.get()
            while set_value<value-np.abs(step) or set_value>value+np.abs(step):
                  set_value+=step
                  device.source.current.set(set_value)           
                  time.sleep(wait)

            device.source.current.set(value)
            
            return True
      except:
            print("Something went wrong")
#%% Initialize
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Current = basis_channel.get()

# savely initiate to starting values
print('initializing...')
time.sleep(1)
print('ramping up V_Emitter...')
ramp_keithley_voltage(V_Emitter_Start, np.abs(current_Emitter_Voltage - V_Emitter_Start)/20, delay_time, emitter_device)
print('ramping up I_Basis...')
ramp_keithley_current(I_Basis_Start, np.abs(current_Basis_Current - I_Basis_Start)/20, delay_time, basis_device)
for i in tqdm(range(20)):
      time.sleep(0.1)
      

#%% 1D sweep of Emitter
emitter_channel.set(V_Emitter_Start)
time.sleep(2)
sweep_Emitter = Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.voltage)
data_Emitter = sweep_Emitter.get_data_set(name="Emitter_Sweep with current sourcing" )
plot = QtPlot()
plot.add(data_Emitter.keithley1_sense_current, subplot=1)
plot.add(data_Emitter.keithley2_source_current, subplot=1)
sweep_Emitter.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Emitter_Sweep at %H_%M_%S o'clock with current sourcing.png"))

#%% 1D sweep of Basis
basis_channel.set(I_Basis_Start)
time.sleep(2)
sweep_Basis =Loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1],sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.voltage)
data_Basis = sweep_Basis.get_data_set(name="Basis Sweep with current sourcing")
plot = QtPlot()
plot.add(data_Basis.keithley1_sense_current, subplot=1)
plot.add(data_Basis.keithley2_source_current, subplot=1)
sweep_Basis.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("Basis Sweep at %H_%M_%S o'clock with current sourcing.png"))

#%% 2D sweep
loop_2d = Loop(emitter_channel.sweep(sweep_Emitter_range[0],sweep_Emitter_range[1],sweep_Emitter_step), delay=2*delay_time).loop(basis_channel.sweep(sweep_Basis_range[0],sweep_Basis_range[1], sweep_Basis_step), delay=delay_time).each(emitter_device.sense.current, emitter_channel, basis_channel, basis_device.sense.voltage)
data_2d = loop_2d.get_data_set(name="2D_Sweep with current sourcing")
plot= QtPlot()
plot.add(data_2d.keithley1_sense_current, subplot=1,name="Emitter current")
plot.add(data_2d.keithley2_sense_voltage, subplot=2,name="Base voltage")
loop_2d.with_bg_task(plot.update).run()
plot.save(filename=time.strftime("2D sweep at %H_%M_%S o'clock with current sourcing.png"))

#%% Set and Sense with buffer
keithley2.reset()
nplc=0.01
keithley2.sense.nplc.set(nplc)
keithley2.sense_function('voltage')
keithley2.output_enabled(True)
VeSS=-1000e-03
IbSS=1e-9
basis_device.source.current.set(IbSS)
emitter_device.source.voltage.set(VeSS)
buffer_name = 'SandS'
n_measurements = 10000
buffer_size = 10000
with keithley2.buffer(buffer_name, buffer_size) as buff1:
    buff1.elements(['relative_time', 'measurement', 'source_value'])
    keithley2.source.sweep_setup(IbSS, IbSS, n_measurements, buffer_name=buff1.buffer_name)
    for i in tqdm(range(100)):
      time.sleep(0.1)  
    data = keithley2.sense.sweep()
    all_data = keithley2.sense.sweep.get_selected()
    print(data)
    print(all_data) 
    result=np.array(all_data)
    result.resize([n_measurements,3])
    with open('noise_result1 with cable1.npy', 'wb') as f:
        np.save(f, result)
    f.close() 
# meas=Measurement(exp=exp, name="Set and Sense" )
# meas.register_parameter(keithley.sense.sweep)
# with meas.run() as datasaver:
#               datasaver.add_result((keithley2.source.sweep_axis, keithley2.source.sweep_axis()),(keithley.sense.sweep.voltage, keithley.sense.sweep()))
#%% Ramping to 0 in a controlled manner
current_Emitter_Voltage = emitter_channel.get()
current_Basis_Current = basis_channel.get()
print('Ramping down...')
time.sleep(1)
print('ramping down V_Emitter...')
ramp_keithley_voltage(0, np.abs(current_Emitter_Voltage)/20, delay_time, emitter_device) #or keithley1.source.voltage.set(0)
print('ramping down I_Basis...')
ramp_keithley_current(0, np.abs(current_Basis_Current)/20, delay_time, basis_device) # or #or keithley2.source.current.set(0)

keithley1.output_enabled(False)
keithley2.output_enabled(False)