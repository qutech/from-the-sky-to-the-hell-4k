# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 17:07:03 2020

@author: levon
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
#%matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import math
 
#p="Y:/GaAs/Barumjan/DATA/HBT/416L-L1-D3/4K with grounding cable/16-12-2020/#012_12-56-01_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
#p = "C:/Users/Baghumyan/Desktop/D_04_20_01/30-11-2020/035_15-49-39_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set.dat"
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L1-D1/4K with grounding cable/16-12-2020/#005_10-57-21_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"

dataset=qc.data.data_set.load_data(p)
print(dataset.read)
#dataset.default_parameter_name()
#dataset.default_parameter_array()
#dataset.read
# A=dataset.default_parameter_array()
# J=dataset.keithley1_sense_current.ndarray
# C=A.ndarray
v_ec = dataset.keithley1_source_voltage.ndarray
v_be = dataset.keithley2_source_voltage.ndarray
i_e = dataset.keithley1_sense_current.ndarray
i_b = dataset.keithley2_sense_current.ndarray

mask=np.logical_and(i_b>=0.1e-9, i_b<=10e-9)
A1=v_ec[mask]
B1=v_be[mask]
A2=v_ec[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
B2=v_be[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
B2=np.sort(B2)
#%% 𝐼_𝐸(𝑉_𝐸𝐶 ,𝑉_𝐵𝐸) and 𝐼_𝐵(𝑉_𝐸𝐶 ,𝑉_𝐵𝐶) # failed watherfall plot
fig,ax = plt.subplots(1, 2, figsize=(8,4), subplot_kw={'projection': '3d'})
X=v_be*1e03
Y=v_ec*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(Z1.min(), Z1.max())
norm2 = mpl.colors.Normalize(Z2.min(), Z2.max())
ax[0].set_xlabel(r"$V_{BE}$ $(mV)$")
ax[0].set_ylabel(r"$V_{EC}$ $(mV)$")
ax[1].set_xlabel(r"$V_{BE}$ $(mV)$")
ax[1].set_ylabel(r"$V_{EC}$ $(mV)$")
p1 = ax[0].plot_surface(X, Y, Z1, rstride=1, cstride=1, linewidth=0, antialiased=True, norm=norm1, cmap=mpl.cm.Blues)
p2 = ax[1].plot_surface(X, Y, Z2, rstride=1, cstride=1, linewidth=0, antialiased=True, norm=norm2, cmap=mpl.cm.Blues)
cb1 = fig.colorbar(p1, ax=ax[0], shrink=0.6)
#cb1.set_label(r"$I_E$")
cb1.ax.set_title(r"$I_E$ $(nA)$")
cb2 = fig.colorbar(p2, ax=ax[1], shrink=0.6)
#cb2.set_label(r"$I_B$")
cb2.ax.set_title(r"$I_B$ $(nA)$")
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
#%% 𝐼_𝐸(𝐼_𝐵) for different 𝑉_𝐸𝐶 values
fig,ax = plt.subplots()
ax.set_xlabel(r"$I_B$ $(nA)$")
ax.set_ylabel(r"$|I_E|$ $(nA)$")
ax.plot()
x=i_b[mask]*1e9
y=abs(i_e[mask]*1e9)
ax.scatter(x,y)
#Order points by their x-value
indexs_to_order_by = x.argsort()
x_ordered = x[indexs_to_order_by]
y_ordered = y[indexs_to_order_by]
ax.plot(x_ordered, y_ordered, label="")
ax.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.0)
#%% 𝛽(𝐼_𝐵),𝑑𝑖𝑓𝑓 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(2, 1, figsize=(8,4))
ax[0].set_xlabel(r"$I_B$ $(nA)$")
ax[0].set_ylabel(r"$\beta$", rotation=0)
ax[1].set_xlabel(r"$I_B$ $(nA)$")
ax[1].set_ylabel(r"$\frac{dI_C}{dI_B}$", rotation=0)
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
wspace=0.2, hspace=0.2)
i_c=(i_e-i_b)
Beta=abs(i_c/i_b)[mask]
x=i_b[mask]*1e9
ax[0].scatter(x, Beta)
indexs_to_order_by = x.argsort()
x_ordered = x[indexs_to_order_by]
y_ordered = Beta[indexs_to_order_by]
ax[0].plot(x_ordered, y_ordered)
#now for 𝑑𝑖𝑓𝑓 𝛽(𝐼_𝐵)
dV_B=np.diff(i_c[mask]*1e9)/np.diff(i_b[mask]*1e9) 
X=x[:-1]
ax[1].scatter(X, dV_B)
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = dV_B[indexs_to_order_BY]
ax[1].plot(X_ordered, Y_ordered)
ax[0].legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.0)
#%% 𝑔_𝑚(𝑉_𝐵𝐸),𝑑𝑖𝑓𝑓 𝑔_𝑚(𝑉_𝐵𝐸)
fig,ax = plt.subplots(2, 1, figsize=(8,4))
ax[0].set_xlabel(r"$V_{BE}$ $(mV)$")
ax[0].set_ylabel(r"$g_m$ $(nS)$")
ax[1].set_xlabel(r"$V_{BE}$ $(mV)$")
ax[1].set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$")
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
wspace=0.2, hspace=0.5)
i_c=((-1*i_e)-i_b) #or directly measure it
𝑔__𝑚=(i_c/v_be)*1e09
𝑔_𝑚=𝑔__𝑚[mask]
x=v_be[mask]*1e03
ax[0].scatter(x, 𝑔_𝑚)
indexs_to_order_by = x.argsort()
x_ordered = x[indexs_to_order_by]
y_ordered = 𝑔_𝑚[indexs_to_order_by]
ax[0].plot(x_ordered, y_ordered)
#now for 𝑑𝑖𝑓𝑓 𝑔_𝑚(𝑉_𝐵𝐸)
d𝑔_𝑚=np.diff(i_c[mask])/np.diff(v_be[mask])*1e09
X=x[:-1]
ax[1].scatter(X, d𝑔_𝑚)
indexs_to_order_BY = X.argsort()
X_ordered = X[indexs_to_order_BY]
Y_ordered = d𝑔_𝑚[indexs_to_order_BY]
ax[1].plot(X_ordered, Y_ordered)
#ax[0].legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.0)
#%% 𝑟_𝜋(𝐼_𝐵), 𝑟_𝑜(𝐼_𝐶)
fig,ax = plt.subplots(1, 2, figsize=(8,4))
ax[0].set_xlabel(r"$I_B$ $(nA)$")
ax[0].set_ylabel(r"$r_\pi$ $(M\Omega)$")
ax[1].set_xlabel(r"$I_C$ $(pA)$")
ax[1].set_ylabel(r"$r_o$ $(k\Omega)$")
plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
wspace=0.4, hspace=0.2)
i_c=((-1*i_e)-i_b)
r_pi=(v_be/i_b)[mask]*1e-06
r_o=(-v_ec/i_c)[mask]*1e-03 #v_ce=-v_ec
x1=i_b[mask]*1e12
x2=i_c[mask]*1e12
ax[0].scatter(x1, r_pi)
indexs_to_order_by1 = x1.argsort()
x_ordered1 = x1[indexs_to_order_by1]
y_ordered1 = r_pi[indexs_to_order_by1]
ax[0].plot(x_ordered1, y_ordered1)
ax[1].scatter(x2, r_o)
indexs_to_order_by2 = x2.argsort()
x_ordered2 = x2[indexs_to_order_by2]
y_ordered2 = r_o[indexs_to_order_by2]
ax[1].plot(x_ordered2, y_ordered2)
#ax[0].legend(bbox_to_anchor=(1.5, -0.2), loc=1, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax[0].xaxis.set_major_formatter(formatter)
ax[1].xaxis.set_major_formatter(formatter)