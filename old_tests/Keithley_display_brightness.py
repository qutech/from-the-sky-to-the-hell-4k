# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 11:11:43 2021

@author: levon
"""

self.add_parameter('display_brightness',
                           get_cmd='DISP:LIGH:STAT?',
                           set_cmd='DISP:LIGH:STAT {}',
                           vals=Bool(ON100, ON75, ON50, ON25, OFF, BLACkout)
                           docstring="The brightness of the display:"
                                     "Full brightness: ON100"
                                     "75 % brightness: ON75"
                                     "50 % brightness: ON50"
                                     "25 % brightness: ON50"
                                     "Display off: OFF"
                                     "Display, key lights, and all indicators off: BLACkout"
                                     )