# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 16:17:27 2020

@author: Baghumyan
"""
import qcodes
import time
from pathlib import Path
from qcodes.interactive_widget import experiments_widget

Expname="AT-AT"
Sampname="SAMPLE NAME"
# and then just run it
experiments_widget()
p = "C:/Users/Baghumyan/Desktop/TEST 3/30-11-2020/30-11-2020 at 15_08_31 o'clock.db"

# you can pass a specific database path
experiments_widget(db=p)

# # you can also pass a specific list of DataSets:
# # say, you're only interested in datasets of a particular experiment
#experiments = qcodes.experiments()
#data_sets = experiments[2].data_sets()
#experiments_widget(data_sets=data_sets)

# # you can change the sorting of the datasets
# # by passing None, "run_id", "timestamp" as sort_by argument:
# experiments_widget(sort_by="timestamp")