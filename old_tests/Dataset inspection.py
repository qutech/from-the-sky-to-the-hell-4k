# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 17:15:32 2020

@author: Baghumyan
"""

#%%
import time
from pathlib import Path
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#from qcodes.instrument_drivers.Harvard.FZJ_Decadac import Decadac
from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2450 import Keithley2450
#from qcodes.instrument_drivers.agilent.Agilent_34400A import Agilent_34400A
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument

from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot
#matplotlib inline
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

import numpy as np
from tqdm import tqdm

from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset

from qcodes.instrument.specialized_parameters import ElapsedTimeParameter
t0=time.time()
def experiment_time(t0=t0):
      t= round((time.time()-t0),4)
      return t
  
#% Load all instruments
qc.Instrument.close_all()
    
#keithley1=Keithley2450('keithley1', 'GPIB1::23::INSTR')
#keithley2=Keithley2450('keithley2', 'GPIB1::18::INSTR')
#agilent = Agilent_34400A('agilent', 'GPIB1::28::INSTR')
#lockin=SR830("lockin",'GPIB1::12::INSTR')

#counter=DummyInstrument(name="dummy2")
#counter.add_parameter('count', set_cmd=None)


p = "C:/Users/Baghumyan/Desktop/TEST 3/30-11-2020/30-11-2020 at 15_08_31 o'clock.db"
#p = "C:/Users/levon/Desktop/test/25-11-2020 at 12_31_01 o_clock.db"


#%% Plan Major

dataset=qc.data.data_set.load_data(p)
print(dataset.read)
#dataset.default_parameter_name()
#dataset.default_parameter_array()
#dataset.read
A=dataset.default_parameter_array()
J=dataset.keithley1_sense_current.ndarray
  # preset_data (Optional[Union[numpy.ndarray, Sequence]]) – Contents of the array, 
  # if already known (for example if this is a setpoint array). shape will be inferred from 
  # this array instead of from the shape argument.
C=A.ndarray

#%% Plan Minor
initialise_or_create_database_at(p)
from qcodes.dataset.experiment_container import experiments
print(experiments())
time.sleep(1)
dataset=qc.dataset.data_set.load_by_id(1)
A=dataset.get_parameter_data("keithley1_sense_current")
K1=A.get("keithley1_sense_current").get("Time")
t=K1=A.get("keithley1_sense_current").get("keithley1_sense_current")
#dataset.guid
#dataset.captured_run_id
#dataset.exp_name
#dataset.sample_name
#dataset.name
#dataset.dependent_parameters
#and so on see https://qcodes.github.io/Qcodes/examples/DataSet/DataSet-class-walkthrough.html

