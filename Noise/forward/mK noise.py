# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 14:23:02 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
#%% define global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 + 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
size=11
labelsize=8
tick_width=0.5
#%%
os.chdir("C:/Users/levon/Desktop/FA_final/Input_reffered_noises")
mypath="C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep"
p=os.listdir(mypath)
p.remove('Discard')
a=p[0]
b=p[1]
c=p[-1]
p=p[2:-1]
p.append(a)
p.append(b)
p.append(c)
del a,b,c
n=len(p)

# import cmd
# cli = cmd.Cmd()
# print("please choose one of {} files given below:".format(n))
# cli.columnize(p, displaywidth=50)
# choice=int(input())

for K in range(n-3):
    #%% Collector current noise spectral density
    #Note, that I use Prof. Bluhm's convention for frequency integration and consequently his noise constant
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/{0:s}".format(p[K]) )
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    v_ec_noise=(mat.get("userdata")["configvals"])[0,0].copy()
    v_ec_noise=v_ec_noise[0,1]*1000
    # FFT_data[np.where(FFT_freq>=3000)]=np.nan
    # FFT_data = FFT_data[~np.isnan(FFT_data)]
    # FFT_freq[np.where(FFT_freq>=3000)]=np.nan
    # FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    gain=0.5e6
    S_omega_i_c=FFT_data
    fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))

    
    #%% Voltaic data
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT Characterization/DATA/sm_hbt_2D_charac_2021_03_29_16_36_22.mat")
    i_c = mat.get("data")[0,1]*-1
    i_b = mat.get("data")[0,0]*-1
    i_e=i_c+i_b
    v_ec_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,0]
    v_ec=np.linspace(v_ec_range[0,0],v_ec_range[0,1] , 101)
    v_ec=np.tile(v_ec, (51,1))
    v_bc_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,1]
    v_bc=np.linspace(v_bc_range[0,0],v_bc_range[0,1] , 51)
    v_bc=np.tile(v_bc, (101,1)).T
    # i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    # i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    # mask=np.isnan(i_b)
    
    # v_ec[mask]=np.nan
    # v_bc[mask]=np.nan
    # i_b[mask]=np.nan
    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
    
    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
    𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
    r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
    
    class Found(Exception): pass
    try:
        for i in np.arange(0, np.size(v_ec[0,:])):
            if v_ec[0,i]*1000==round(v_ec_noise[0,0],1) or v_ec[0,i]*1000==round(v_ec_noise[0,0],0): 
                raise Found
    except Found: print (i)
    delta_i_c=( np.gradient(i_c,axis=1) )[0,i]
    
    i_b=i_b[0,i]*np.ones(N)
    i_c=i_c[0,i]*np.ones(N)
    #v_be=v_be[0,i]*np.ones(N)
    g_m=g_m[0,i]*np.ones(N)
    Pwr=Pwr[0,i]*np.ones(N)
    r_pi=r_pi[0,i]*np.ones(N)
    r_set=2e5
    R=r_set*r_pi/(r_set+r_pi)
    
    F=1
    S_b_shot= np.sqrt(2*e*i_b*F)
    S_b_shot_amp=S_b_shot*g_m*R
#%%the plot    
    # ax[0].plot(f[mask],S_omega[mask],'og')
    ax.plot(f,np.sqrt(S_omega_i_c)/gain,'g',lw=0.3, 
            label=r"$P$=%.1f (nW), $I_\mathrm{B}$=%.1f (nA)" % (Pwr[0]*1e9, i_b[0]*1e9) )
    
    mat_basel_amp = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/{0:s}".format(p[-1] ))
    S_basel_amp=(mat_basel_amp.get("spectra")["FFTdata"])[0,0].copy()
    S_basel_amp=S_basel_amp[:,0]
    #ax.plot(f,np.sqrt(S_omega_i_c-S_basel_amp)/gain,'palegreen',lw=0.3, label=r"other noises substracted" % v_ec_noise)
    
    ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=size)
    ax.set_xlabel(r"$f$ (Hz)", size=size)
    #ax.set_title("Noise spectral density of collecotor current", fontsize=20, fontname='serif', color="blue")
    ax.grid()
    ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
    ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(2)
      ax.tick_params(width=tick_width, length=7, which="major",direction="in")
      ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
    ax.set_yscale('log')
    ax.set_xscale('log')
    Z_o=50
    T_n=S_omega_i_c*Z_o/(k*gain*gain)
    print("Collector noise temperature is {} Kelvin".format(T_n))
    
    ax.plot(f,S_b_shot_amp, lw=0.5, color="maroon", label="Base amplifed shot noise")
    
    
    S_set_shot_amp=np.sqrt(2*e*F*delta_i_c*g_m*R)
    ax.plot(f,S_set_shot_amp, lw=0.5, color="navy", label="SET amplifed shot noise")
    
    S_c_shot=np.sqrt(2*e*i_c*F)
    ax.plot(f, S_c_shot,'red', lw=0.5, label="Collector shot noise")
    
    S_total=S_b_shot_amp**2+S_set_shot_amp**2+S_c_shot**2
    ax.plot(f, np.sqrt(S_total),'darkorange', lw=1, label="Total output shot noise")
    
    #%% wrong
    # Beta=integrate.simps(S_omega_i_c/(gain**2), f)
    # SNR=i_c[0]**2/Beta
    # print("Signal-to-noise ratio is {}".format(SNR))
    # #%%linear regression #1
    # # A_test=((S_omega_i_c)/(gain**2) )[0]*f[0]
    # # ax.plot(f, np.sqrt(A_test/f) , "pink", lw=1, label=r"1/f noise, $\mathrm{\alpha/2=0.5}$")
    # x=np.log(f).reshape(-1, 1)
    # y=np.log(np.sqrt(S_omega_i_c)/gain).reshape(-1, 1)
    # model= LinearRegression().fit(x,y)
    # r_sq = model.score(x, y)
    # print('coefficient of determination:', r_sq)
    # print('intercept:', model.intercept_)
    # print('slope:', model.coef_)
    # a=np.exp(model.intercept_)
    # b=model.coef_
    
    # pinky=a*np.power(f,b)
    # pinky=pinky[0,:]
    # ax.plot(f, pinky,'lime',lw=1, label=r"linear regression #1, $\mathrm{\alpha/2=%.2f }$" %-b )
    #%%linear regression 2
    
    # old = np.seterr(invalid='ignore')
    # x=np.log(f).reshape(-1, 1)
    # y=np.log( np.sqrt(S_omega_i_c/(gain**2)-S_total) ).reshape(-1, 1)
    # ax.plot(f, np.sqrt(S_omega_i_c/(gain**2)-S_total), lw=1, color="cyan", label="Reduced noise")
    # # x=x[np.isnan(y)]
    # # y=y[np.isnan(y)]
    # model= LinearRegression().fit(x,y)
    # r_sq = model.score(x, y)
    # print('coefficient of determination:', r_sq)
    # print('intercept:', model.intercept_)
    # print('slope:', model.coef_)
    # a=np.exp(model.intercept_)
    # b=model.coef_
    # pinky=a*np.power(f,b)
    # pinky=pinky[0,:]
    # ax.plot(f, pinky,'aqua',lw=1, label=r"linear regression #2, $\mathrm{\alpha/2=%.2f }$" %-b )
    
    ax.tick_params(axis='x', labelsize=labelsize)
    ax.tick_params(axis='y', labelsize=labelsize)
    xcoords = [100e3, 100e3]
    for xc in xcoords:
        plt.axvline(x=xc, color="maroon",   linestyle="--")
    leg=ax.legend(bbox_to_anchor=(0, 1.04), loc=3, borderaxespad=0.0, prop={'size': labelsize})
    for line in leg.get_lines():
        line.set_linewidth(1)
    plt.text(0.61, 0.85, r"$f$ cutoff", rotation=90, transform=ax.transAxes, verticalalignment='center', size=labelsize, family="serif", color="maroon")
    
    # print("Do you want to save the plot?")
    # ans = input()
    # if ans == "1":
    #         plt.savefig("mK_fa_noises_{0:.1f}mV.pdf".format(v_ec_noise[0,0]) )
    #         plt.savefig("mK_fa_noises_{0:.1f}mV.png".format(v_ec_noise[0,0]) )
    # if ans == "0": pass
    plt.savefig("mK_fa_noises_{0:.1f}mV.pdf".format(v_ec_noise[0,0]), bbox_inches='tight' )
    plt.savefig("mK_fa_noises_{0:.1f}mV.png".format(v_ec_noise[0,0]), bbox_inches='tight' )
    K+=1
