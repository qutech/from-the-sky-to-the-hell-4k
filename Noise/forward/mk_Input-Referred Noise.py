# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 09:23:59 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
os.chdir("C:/Users/levon/Desktop/FA_final/Input_reffered_noises")
#%% Voltaic data
mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT Characterization/DATA/sm_hbt_2D_charac_2021_03_29_16_36_22.mat")
i_c = mat.get("data")[0,1]*-1
i_b = mat.get("data")[0,0]*-1
i_e=i_c+i_b
v_ec_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,0]
v_ec=np.linspace(v_ec_range[0,0],v_ec_range[0,1] , 101)
v_ec=np.tile(v_ec, (51,1))
v_bc_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,1]
v_bc=np.linspace(v_bc_range[0,0],v_bc_range[0,1] , 51)
v_bc=np.tile(v_bc, (101,1)).T
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


#%% Collector current noise spectral density
mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/spectrum__20210421_160912.mat")
FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
FFT_data=FFT_data[:,0]
FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
FFT_freq=FFT_freq[:,0]
timestep=(mat.get("spectra")["timestep"])[0,0].copy()
timestep=timestep[0,0]
hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
hardwareSampFreq=hardwareSampFreq[0,0]
v_ec_noise=(mat.get("userdata")["configvals"])[0,0].copy()
v_ec_noise=v_ec_noise[0,1]*1000
# FFT_data[np.where(FFT_freq>=29814)]=np.nan
# FFT_data = FFT_data[~np.isnan(FFT_data)]
# FFT_freq[np.where(FFT_freq>=29814)]=np.nan
# FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
N=FFT_freq.size
f_s=hardwareSampFreq #sampling frquency
delta_f=f_s/N #resolution of the frequency spectrum
B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
f=FFT_freq
T=N/f_s
gain=0.5e6
S_omega_i_c=FFT_data
fig,ax = plt.subplots(1, 1, figsize=(12,12))
# ax[0].plot(f[mask],S_omega[mask],'og')

for i in np.arange(0, np.size(v_ec[0,:])):
    if v_ec[0,i]*1000==round(v_ec_noise[0,0],1):
        break
    else: continue
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta=AC_Beta[0,i]
Beta=i_c/i_b
Beta=Beta[0,i]

ax.plot(f,np.sqrt(S_omega_i_c)/(gain*AC_Beta),'forestgreen',lw=0.3, label=r"$V_{EC}$=%.1f (mV), AC gain" % v_ec_noise)
ax.plot(f,np.sqrt(S_omega_i_c)/(gain*Beta),'navy',lw=0.3, label=r"$V_{EC}$=%.1f (mV), DC gain" % v_ec_noise)

ax.set_ylabel(r"Input-Referred Noise (A/$\mathrm{\sqrt{Hz}}$)", size=22)
ax.set_xlabel(r"f (Hz)", size=22)
#ax.set_title("Noise spectral density of collecotor current", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.grid()
#ax.xaxis.set_major_formatter(formatter)
#.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.set_yscale('log')
ax.set_xscale('log')

ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)
xcoords = [100e3, 100e3]
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")
leg=ax.legend(loc=3, borderaxespad=0.0,prop={'size': 14})
for line in leg.get_lines():
    line.set_linewidth(2)
plt.text(0.64, 0.85, "amplifier cutoff", rotation=90, transform=ax.transAxes, verticalalignment='center', size=20, family="serif", color="maroon")
ans = input()
if ans == "1":
        plt.savefig("mK__Input-Referred Collector Noise_noise.pdf") 
        plt.savefig("mK__Input-Referred Collector Noise_noise.png")
if ans == "0": pass