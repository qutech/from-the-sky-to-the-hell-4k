# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 17:02:51 2021

@author: levon
"""



import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
os.chdir("C:/Users/levon/Desktop/FA_final/Input_reffered_noises")

mypath="C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep"
p=os.listdir(mypath)
p.remove('Discard')
a=p[0]
b=p[1]
c=p[-1]
p=p[2:-1]
p.append(a)
p.append(b)
p.append(c)
del a,b,c
n=len(p)

gain=0.5e6

n=n-3

S_data_DC=np.zeros(n)
S_data_AC=np.zeros(n)
Pwr_data=np.zeros(n)
i_test=np.zeros(n)
for k in range(n):
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/{0:s}".format( p[k] ) )
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    v_ec_noise=(mat.get("userdata")["configvals"])[0,0].copy()
    v_ec_noise=v_ec_noise[0,1]*1000
    FFT_data[np.where(FFT_freq>=100e3)]=np.nan
    FFT_data = FFT_data[~np.isnan(FFT_data)]
    FFT_freq[np.where(FFT_freq>=100e3)]=np.nan
    FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT Characterization/DATA/sm_hbt_2D_charac_2021_03_29_16_36_22.mat")
    i_c = mat.get("data")[0,1]*-1
    i_b = mat.get("data")[0,0]*-1
    i_e=i_c+i_b
    v_ec_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,0]
    v_ec=np.linspace(v_ec_range[0,0],v_ec_range[0,1] , 101)
    v_ec=np.tile(v_ec, (51,1))
    v_bc_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,1]
    v_bc=np.linspace(v_bc_range[0,0],v_bc_range[0,1] , 51)
    v_bc=np.tile(v_bc, (101,1)).T
    i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
    mask=np.isnan(i_b)

    v_ec[mask]=np.nan
    v_bc[mask]=np.nan
    i_b[mask]=np.nan
    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)

    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)

    for i in np.arange(0, np.size(v_ec[0,:])):
        if v_ec[0,i]*1000==round(v_ec_noise[0,0],1):
            break
        if v_ec[0,i]*1000==round(v_ec_noise[0,0],0):
            break
        else: continue

    Pwr=Pwr[0,i]
    AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
    AC_Beta=AC_Beta[0,i]
    Beta=i_c/i_b
    Beta=Beta[0,i]
    
    Pwr_data[k]=Pwr
    S_data_DC[k]=np.nanmin(S_omega_i_c)/Beta**2
    S_data_AC[k]=np.nanmin(S_omega_i_c)/AC_Beta**2
    i_test[k]=i
    k+=1

fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_ylabel(r"Miniumum Input-Referred Noise (A/$\mathrm{\sqrt{Hz}}$)", size=22)
ax.set_xlabel(r"P (W)", size=22)
#ax.set_title("Noise spectral density of collecotor curren for different emmiter voltages", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
#ax.text(0, 0.1, "Text label", fontsize=14, family="serif")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
ax.plot(Pwr_data, np.sqrt(S_data_DC)/gain, marker="o", color="indigo", label=r"DC gain used",)
ax.plot(Pwr_data, np.sqrt(S_data_AC)/gain, marker="o", color="olivedrab", label=r"AC gain used",)
# leg=ax.legend(loc=3, borderaxespad=0.0)
# for line in leg.get_lines():
#     line.set_linewidth(2)

ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)   
leg=ax.legend(loc=1, borderaxespad=0.0)
for line in leg.get_lines():
    line.set_linewidth(2)
    
print("Do you want to save the plot?")
ans = input()
if ans == "1":
        plt.savefig("mK_NSDmin_vs_power.pdf") 
        plt.savefig("mK_NSDmin_vs_power.png")
if ans == "0": pass