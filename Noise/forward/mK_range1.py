# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 12:34:59 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from scipy.stats import gaussian_kde as kde
from sklearn.linear_model import LinearRegression
os.chdir("C:/Users/levon/Desktop/Noise plots/range1_plots/weighted_version2")

mypath="C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep"
p=os.listdir(mypath)
p.remove('Discard')
p=p[2:-1]
n=len(p)

import cmd
cli = cmd.Cmd()
print("please choose one of {} files given below:".format(n))
cli.columnize(p, displaywidth=50)
choice=int(input())
#%% Collector current noise spectral density
#Note, that I use Prof. Bluhm's convention for frequency integration and consequently his noise constant
mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/{0:s}".format(p[choice]) )
FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
FFT_data=FFT_data[:,0]
FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
FFT_freq=FFT_freq[:,0]
timestep=(mat.get("spectra")["timestep"])[0,0].copy()
timestep=timestep[0,0]
hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
hardwareSampFreq=hardwareSampFreq[0,0]
v_ec_noise=(mat.get("userdata")["configvals"])[0,0].copy()
v_ec_noise=v_ec_noise[0,1]*1000
FFT_data[np.where(FFT_freq>=3000)]=np.nan
FFT_data = FFT_data[~np.isnan(FFT_data)]
FFT_freq[np.where(FFT_freq>=3000)]=np.nan
FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
N=FFT_freq.size
f_s=hardwareSampFreq #sampling frquency
delta_f=f_s/N #resolution of the frequency spectrum
B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
f=FFT_freq
T=N/f_s
gain=0.5e6
S_omega_i_c=FFT_data
fig,ax = plt.subplots(1, 1, figsize=(12,12))
# ax[0].plot(f[mask],S_omega[mask],'og')
ax.plot(f,np.sqrt(S_omega_i_c)/gain,'g',lw=0.3, label=r"$V_{EC}$=%.1f (mV)" % v_ec_noise)
ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.set_xlabel(r"f (Hz)", size=20)
#ax.set_title("Noise spectral density of collecotor current", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.grid()
#ax.xaxis.set_major_formatter(formatter)
#.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.set_yscale('log')
ax.set_xscale('log')

# Z_o=50
# T_n=S_omega_i_c*Z_o/(k*gain*gain)
# print("Collector noise temperature is {} Kelvin".format(T_n))

#%% Voltaic data
mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT Characterization/DATA/sm_hbt_2D_charac_2021_03_29_16_36_22.mat")
i_c = mat.get("data")[0,1]*-1
i_b = mat.get("data")[0,0]*-1
i_e=i_c+i_b
v_ec_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,0]
v_ec=np.linspace(v_ec_range[0,0],v_ec_range[0,1] , 101)
v_ec=np.tile(v_ec, (51,1))
v_bc_range = ( ( mat.get("scan")["loops"][0,0] )["rng"] )[0,1]
v_bc=np.linspace(v_bc_range[0,0],v_bc_range[0,1] , 51)
v_bc=np.tile(v_bc, (101,1)).T
i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)

for i in np.arange(0, np.size(v_ec[0,:])):
    if v_ec[0,i]*1000==round(v_ec_noise[0,0],1):
        break
    if v_ec[0,i]*1000==round(v_ec_noise[0,0],0):
        break
    else: continue

delta_i_c=( np.gradient(i_c,axis=1) )[0,i]

i_b=i_b[0,i]*np.ones(N)
i_c=i_c[0,i]*np.ones(N)
#v_be=v_be[0,i]*np.ones(N)
g_m=g_m[0,i]*np.ones(N)
r_pi=r_pi[0,i]*np.ones(N)
r_set=2e5
R=r_set*r_pi/(r_set+r_pi)


F=1
S_b_shot= np.sqrt(2*e*i_b*F)
S_b_shot_amp=S_b_shot*g_m*R
# ax.plot(f,S_b_shot_amp, lw=1, color="maroon", label="Base amplifed shot noise")

S_set_shot_amp=np.sqrt(2*e*F*delta_i_c*g_m*R)
# ax.plot(f,S_set_shot_amp, lw=1, color="navy", label="SET amplifed shot noise")

S_c_shot=np.sqrt(2*e*i_c*F)
# ax.plot(f, S_c_shot,'red', lw=1, label="Collector shot noise")

S_total=S_b_shot_amp**2+S_set_shot_amp**2+S_c_shot**2
# ax.plot(f, np.sqrt(S_total),'darkorange', lw=3, label="Total output shot noise")


#%%linear regression 

S_reduced=S_omega_i_c/(gain**2)-S_total
#sample_weight=kde(np.sqrt(S_reduced)) 
    
#%%
# for i in np.arange(0,100):
#     peaks=scipy.signal.find_peaks(S_reduced,threshold=np.power(0.1e-11,2))
#     ax.scatter(f[peaks[0]], np.sqrt( S_reduced[peaks[0]] ), lw=1)
#     S_reduced[peaks[0]]=np.nan
#     S_reduced = S_reduced[~np.isnan(S_reduced)]
#     f[peaks[0]]=np.nan
#     f = f[~np.isnan(f)]
#     i+=i
 #Z = np.reshape(sample_weight(f).T, X.shape)

line_points=plt.ginput(2)
A=line_points[0]
B=line_points[1]
k=np.log(A[1]/B[1])/np.log(A[0]/B[0])
B_free=A[1]/np.power(A[0],k)
x_points=f
y_points=B_free*np.power(x_points,k)
#ax.plot(x_points,y_points)
mask1=y_points>=np.sqrt(S_reduced)
#ax.scatter(f[mask1],np.sqrt(S_reduced)[mask1],color="k",marker="+")
f=f[mask1]
S_reduced=S_reduced[mask1]
 
#%%
N=f.size
sample_weight = np.ones(N)
unit=np.log(f[-1])-np.log(f[-2]) #rhis is weght of 1
for i in np.arange(N-1):
    sample_weight[i]=(np.log(f[i+1])-np.log(f[i]) )/unit
sample_weight = sample_weight / sample_weight.max()  
#%%

   
ax.plot(f, np.sqrt(S_reduced), lw=1, color="blue", label="Reduced noise")    
x=np.log(f).reshape(-1, 1)
y=np.log( np.sqrt(S_reduced) ).reshape(-1, 1)
model= LinearRegression().fit(x,y, sample_weight=sample_weight)
r_sq = model.score(x, y)
print('coefficient of determination:', r_sq)
print('intercept:', model.intercept_)
print('slope:', model.coef_)
a=np.exp(model.intercept_)
b=model.coef_
pinky=a*np.power(f,b)
pinky=pinky[0,:]
ax.plot(f, pinky,'navy',lw=1, label=r"linear regression, $\mathrm{\alpha/2=%.2f }$" %-b )



leg=ax.legend(loc=1, borderaxespad=0.0)
for line in leg.get_lines():
    line.set_linewidth(2)


print("Do you want to save the plot?")
ans = input()
if ans == "1":
        plt.savefig("mK_Range1_noise {0:.1f}mV.pdf".format(v_ec_noise[0,0]) )
        plt.savefig("mK_Range1_noise {0:.1f}mV.png".format(v_ec_noise[0,0]) )
if ans == "0": pass