# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 18:12:27 2021

@author: levon
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 14:56:03 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
os.chdir("C:/Users/levon/Desktop/saturation_noise_plots/left")

mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_left"
p=os.listdir(mypath)
p=p[0:4]
n=len(p)

#%%
fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.set_xlabel(r"f (Hz)", size=20)
#ax.set_title("Noise spectral density of collecotor curren for different emmiter voltages", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
#ax.text(0, 0.1, "Text label", fontsize=14, family="serif")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
NUM_COLORS = n
cm = plt.get_cmap("plasma")
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for i in range(n):
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_left/{0:s}".format( p[i] ) )
    gain=(mat.get("userdata")["amp_gain"])[0,0].copy()
    gain=gain[0,0]
    gain=float(gain)
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]

    # FFT_data[np.where(FFT_freq>=500000)]=np.nan
    # FFT_data = FFT_data[~np.isnan(FFT_data)]
    # FFT_freq[np.where(FFT_freq>=500000)]=np.nan
    # FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    ax.plot(f,np.sqrt(S_omega_i_c)/gain,lw=0.3)
    n+=1
    del mat
    
xcoords = [165]
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")
# leg=ax.legend(loc=3, borderaxespad=0.0)
# for line in leg.get_lines():
#     line.set_linewidth(2)
plt.text(0.2, 0.85, "amplifier cutoff????", rotation=90, transform=ax.transAxes, verticalalignment='center', size=20, family="serif", color="maroon")

print("Do you want to save the plot?")
ans = input()
if ans == "1":
        plt.savefig("mK_without_HBT_noise_v1.pdf")
        plt.savefig("mK_without_HBT_noise_v2.png")
if ans == "0": pass
