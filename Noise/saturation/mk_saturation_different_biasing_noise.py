# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 14:56:03 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
#%% define global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 390
size=11
labelsize=8
tick_width=0.5
#%%
os.chdir("C:/Users/levon/Desktop/S_final/raw_noise")
mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_sat"
p=os.listdir(mypath)
adj=p[-1]
p1=p[0:10]
p1.append(adj)
del adj
p2=p[10:-1]

print("Please chose the constant V_BC (0) or V_EC (1) zones")
ans = input()
if ans == "0": p=p1
elif ans == "1": p=p2
else: p=p1+p2
n=len(p)


#%%
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=size)
ax.set_xlabel(r"$f$ (Hz)", size=size)
#ax.set_title("Noise spectral density of collecotor curren for different emmiter voltages", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="in")
ax.tick_params(width=tick_width, length=4, which="minor",direction="in")
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
#ax.text(0, 0.1, "Text label", fontsize=14, family="serif")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
NUM_COLORS = n
cm = plt.get_cmap("inferno")
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for i in range(n):
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_sat/{0:s}".format( p[i] ) )
    gain=(mat.get("userdata")["amp_gain"])[0,0].copy()
    gain=gain[0,0]
    gain=float(gain)
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    configvals=(mat.get("userdata")["configvals"])[0,0].copy()
    v_eb_noise=configvals[0,23]*1000
    v_eb_noise=v_eb_noise[0,0]
    v_cb_noise=configvals[0,22]*1000
    v_cb_noise=v_cb_noise[0,0]
    v_ec_noise=v_eb_noise-v_cb_noise
    # FFT_data[np.where(FFT_freq>=500000)]=np.nan
    # FFT_data = FFT_data[~np.isnan(FFT_data)]
    # FFT_freq[np.where(FFT_freq>=500000)]=np.nan
    # FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    ax.plot(f,np.sqrt(S_omega_i_c)/gain,lw=0.3,
            label=r"$V_\mathrm{{EC}}$={0:.0f} (mV), $V_\mathrm{{BC}}$={1:.0f} (mV)".format(v_ec_noise, -1*v_cb_noise))
    # test_f=f.copy()
    # test_f[np.where(FFT_freq>=2e4)]=np.nan
    # test_f = test_f[~np.isnan(test_f)]
    # S_test=S_omega_i_c.copy()
    # S_test[np.where(FFT_freq>=2e4)]=np.nan
    # S_test = S_test[~np.isnan(S_test)]
    # S_test=np.nanmedian(S_test[-750:])
    # ax.plot(test_f,np.sqrt(S_test*np.ones(test_f.shape))/gain, color="maroon")
    n+=1
    
xcoords = [2e4]
ax.tick_params(axis='x', labelsize=labelsize)
ax.tick_params(axis='y', labelsize=labelsize)
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")
leg=ax.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize-2})
for line in leg.get_lines():
    line.set_linewidth(2)
plt.text(0.53, 0.2, "frequency cutoff", rotation=90, transform=ax.transAxes,
         verticalalignment='center', size=labelsize, family="serif", color="maroon")

print("Do you want to save the plot?")
Ans = input()
if Ans == "1":
        plt.savefig("Sat_mK_collector_different_biased_noise_v{0:s}.pdf".format(ans), bbox_inches='tight' ) 
        plt.savefig("Sat_mK_collector_different_biased_noise_v{0:s}.png".format(ans), bbox_inches='tight' )
if Ans == "0": pass
