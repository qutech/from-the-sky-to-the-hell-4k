# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 11:07:15 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
os.chdir("C:/Users/levon/Desktop/S_final/range1_plots/fits")

mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_sat"
p=os.listdir(mypath)
adj=p[-1]
p1=p[0:10]
p1.append(adj)
del adj
p2=p[10:-1]
p2=p2[5:]

p=p1+p2
n=len(p)

import cmd
cli = cmd.Cmd()
print("please choose one of {} files given below:".format(n))
cli.columnize(p, displaywidth=50)
#k=int(input())
for K in range(n):
        
    #%% Collector current noise spectral density
    #Note, that I use Prof. Bluhm's convention for frequency integration and consequently his noise constant
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_sat/{0:s}".format( p[K] ) )
    gain=(mat.get("userdata")["amp_gain"])[0,0].copy()
    gain=gain[0,0]
    gain=float(gain)
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    configvals=(mat.get("userdata")["configvals"])[0,0].copy()
    v_eb_noise=configvals[0,23]*1000
    v_eb_noise=v_eb_noise[0,0]
    v_cb_noise=configvals[0,22]*1000
    v_cb_noise=v_cb_noise[0,0]
    v_bc_noise=-v_cb_noise
    v_ec_noise=v_eb_noise-v_cb_noise
    FFT_data[np.where(FFT_freq>=3000)]=np.nan
    FFT_data = FFT_data[~np.isnan(FFT_data)]
    FFT_freq[np.where(FFT_freq>=3000)]=np.nan
    FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    fig,ax = plt.subplots(1, 1, figsize=(12,12))
    # ax[0].plot(f[mask],S_omega[mask],'og')
    ax.plot(f,np.sqrt(S_omega_i_c)/gain, color="olivedrab",lw=0.3, label=r"$V_{{EC}}$={0:.0f} (mV), $V_{{BC}}$={1:.0f} (mV)".format(v_ec_noise, -1*v_cb_noise))
    ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=22)
    ax.set_xlabel(r"f (Hz)", size=22)
    #ax.set_title("Noise spectral density of collecotor current", fontsize=20, fontname='serif', color="blue")
    #formatter = mpl.ticker.ScalarFormatter(useMathText=True)
    ax.grid()
    #ax.xaxis.set_major_formatter(formatter)
    #.yaxis.set_major_formatter(formatter)
    ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
    ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(2)
    ax.tick_params(width=1.5, length=12, which="major",direction="in")
    ax.tick_params(width=1.5, length=6, which="minor",direction="in")
    ax.set_yscale('log')
    ax.set_xscale('log')
    
    # Z_o=50
    # T_n=S_omega_i_c*Z_o/(k*gain*gain)
    # print("Collector noise temperature is {} Kelvin".format(T_n))
    
    #%% Voltaic data
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/mK_saturation/levon_sm_hbt_2D_charac_qdac_2021_07_21_20_54_23.mat")
    i_c = mat.get("i_c").T*-1.
    i_b = mat.get("i_b").T*-1
    i_e=i_c+i_b
    v_cb=mat.get("v_cb_full").T
    v_bc=-v_cb
    v_eb=mat.get("v_eb_full").T
    v_be=-v_eb
    v_ec=v_eb-v_cb
    
    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
    𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
    r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
    
    
    class Found(Exception): pass
    try:
        for i in np.arange(0, np.size(v_ec[:,0])):
            for j in np.arange(0, np.size(v_ec[0,:])):
                if ( (v_ec[i,j]*1000==round(v_ec_noise,1)) and (v_bc[i,j]*1000==round(v_bc_noise,1) ) ): raise Found
    except Found: print (i, j)
        
    
    delta_i_c=( np.gradient(i_c,axis=1) )[i,j]
    
    i_b=i_b[i,j]*np.ones(N)
    i_c=i_c[i,j]*np.ones(N)
    
    g_m=g_m[i,j]*np.ones(N)
    r_pi=r_pi[i,j]*np.ones(N)
    r_set=2e5
    R=r_set*r_pi/(r_set+r_pi)
        
    F=1
    S_b_shot= np.sqrt(2*e*i_b*F)
    S_b_shot_amp=S_b_shot*g_m*R
    # ax.plot(f,S_b_shot_amp, lw=1, color="maroon", label="Base amplifed shot noise")
    
    S_set_shot_amp=np.sqrt(2*e*F*delta_i_c*g_m*R)
    # ax.plot(f,S_set_shot_amp, lw=1, color="navy", label="SET amplifed shot noise")
    
    S_c_shot=np.sqrt(2*e*i_c*F)
    # ax.plot(f, S_c_shot,'red', lw=1, label="Collector shot noise")
    
    S_total=S_b_shot_amp**2+S_set_shot_amp**2+S_c_shot**2
    # ax.plot(f, np.sqrt(S_total),'darkorange', lw=3, label="Total output shot noise")
    

    #%%weighing
    S_reduced=S_omega_i_c/(gain**2)-S_total

    line_points=plt.ginput(2)
    A=line_points[0]
    B=line_points[1]
    k=np.log(A[1]/B[1])/np.log(A[0]/B[0])
    B_free=A[1]/np.power(A[0],k)
    x_points=f
    y_points=B_free*np.power(x_points,k)
    #ax.plot(x_points,y_points)
    mask1=y_points>=np.sqrt(S_reduced)
    #ax.scatter(f[mask1],np.sqrt(S_reduced)[mask1],color="k",marker="+")
    f=f[mask1]
    S_reduced=S_reduced[mask1]
    
    N=f.size
    sample_weight = np.ones(N)
    unit=np.log(f[-1])-np.log(f[-2]) #rhis is weght of 1
    for l in np.arange(N-1):
        sample_weight[l]=(np.log(f[l+1])-np.log(f[l]) )/unit
    sample_weight = sample_weight / sample_weight.max()  
 
    #%%linear regression 
    
    x=np.log(f).reshape(-1, 1)
    y=np.log( np.sqrt(S_reduced) ).reshape(-1, 1)
    ax.plot(f, np.sqrt(S_reduced), lw=1, color="blue", label="Reduced noise")    
    model= LinearRegression().fit(x,y, sample_weight=sample_weight)
    r_sq = model.score(x, y)
    print('coefficient of determination:', r_sq)
    print('intercept:', model.intercept_)
    print('slope:', model.coef_)
    a=np.exp(model.intercept_)
    b=model.coef_
    pinky=a*np.power(f,b)
    pinky=pinky[0,:]
    ax.plot(f, pinky,'navy',lw=1, label=r"linear regression, $\mathrm{\alpha/2=%.2f }$" %-b )
    
    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)     
    leg=ax.legend(loc=3, borderaxespad=0.0, prop={'size': 14})
    for line in leg.get_lines():
        line.set_linewidth(2)
    
    
    # print("Do you want to save the plot?")
    # ans = input()
    # if ans == "1":
    #         plt.savefig("mK_Range1_regression_noise_point{0:.1f}.pdf".format(k) )
    #         plt.savefig("mK_Range1_regression_noise_point{0:.1f}.png".format(k) )
    # if ans == "0": pass
    plt.savefig("mK_saturation_Range1_regression_noise_point{0:.0f}.pdf".format(K) )
    plt.savefig("mK_saturation_Range1_regression_noise_point{0:.0f}.png".format(K) )
    K=K+1