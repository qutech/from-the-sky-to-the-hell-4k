# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 15:36:05 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from scipy import interpolate
def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

os.chdir("C:/Users/levon/Desktop/S_Final/input_reffered_noises")

fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_ylabel(r"Input-Referred Noise (A/$\mathrm{\sqrt{Hz}}$)", size=22)
ax.set_xlabel(r"f (Hz)", size=22)
#ax.set_title("Noise spectral density of collecotor current", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.grid()
#ax.xaxis.set_major_formatter(formatter)
#.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.set_yscale('log')
ax.set_xscale('log')


mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_sat"
p=os.listdir(mypath)
adj=p[-1]
p1=p[0:10]
p1.append(adj)
del adj
p2=p[10:-1]
p2=p2[5:]
print(r"Please chose the constant $V_BC$ (0) or $V_EC$ (1) zones")
ans = input()
if ans == "0": p=p1
elif ans == "1": p=p2
else: p=p1+p2
n=len(p)



NUM_COLORS = n
cm = plt.get_cmap("inferno")
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for k in range(n):
#%% Voltaic data
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/mK_saturation/levon_sm_hbt_2D_charac_qdac_2021_07_21_20_54_23.mat")
    i_c = mat.get("i_c").T*-1.
    i_b = mat.get("i_b").T*-1
    v_cb=mat.get("v_cb_full").T
    v_bc=-v_cb
    v_eb=mat.get("v_eb_full").T
    v_be=-v_eb
    v_ec=v_eb-v_cb

    v_be=v_bc-v_ec
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)

# for i in np.arange(101):
#     nans, x= nan_helper((i_b[i,:]))
#     (i_b[i,:])[nans]= np.interp(x(nans), x(~nans), (i_b[i,:])[~nans])
# for i in np.arange(101):
#     nans, x= nan_helper((i_c[i,:]))
#     (i_c[i,:])[nans]= np.interp(x(nans), x(~nans), (i_c[i,:])[~nans])


    i_e=i_c+i_b

#%% Collector current noise spectral density
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_sat/{0:s}".format( p[k] ) )
    gain=(mat.get("userdata")["amp_gain"])[0,0].copy()
    gain=gain[0,0]
    gain=float(gain)
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    configvals=(mat.get("userdata")["configvals"])[0,0].copy()
    v_eb_noise=configvals[0,23]*1000
    v_eb_noise=v_eb_noise[0,0]
    v_cb_noise=configvals[0,22]*1000
    v_cb_noise=v_cb_noise[0,0]
    v_bc_noise=-v_cb_noise
    v_ec_noise=v_eb_noise-v_cb_noise
    # FFT_data[np.where(FFT_freq>=500000)]=np.nan
    # FFT_data = FFT_data[~np.isnan(FFT_data)]
    # FFT_freq[np.where(FFT_freq>=500000)]=np.nan
    # FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    
    class Found(Exception): pass
    try:
        for i in np.arange(0, np.size(v_ec[:,0])):
            for j in np.arange(0, np.size(v_ec[0,:])):
                if ( (v_ec[i,j]*1000==round(v_ec_noise,1)) and (v_bc[i,j]*1000==round(v_bc_noise,1) ) ): raise Found
    except Found: print (i, j)
    Beta=i_c/i_b
    Beta=Beta[i,j]

    ax.plot(f,np.sqrt(S_omega_i_c)/(gain*Beta),lw=0.3, label=r"$V_{{EC}}$={0:.0f} (mV), $V_{{BC}}$={1:.0f} (mV), DC gain".format(v_ec_noise, -1*v_cb_noise))
    k+=1

xcoords = [2e4]
ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")
leg=ax.legend(loc=3, borderaxespad=0.0, prop={'size': 14})
for line in leg.get_lines():
    line.set_linewidth(2)
plt.text(0.53, 0.15, "amplifier cutoff", rotation=90, transform=ax.transAxes, verticalalignment='center', size=20, family="serif", color="maroon")


print("Do you want to save the plot?")
ans2 = input()
if ans2 == "1":
        plt.savefig("mK_DC_saturation_Input-Referred Collector Noise_noise_point{0:s}_all.pdf".format(ans)) 
        plt.savefig("mK_DC_saturation_Input-Referred Collector Noise_noise_point{0:s}_all.png".format(ans))
if ans2 == "0": pass