# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:11:11 2021

@author: levon
"""

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
import seaborn as sns
import math
from pprint import pprint
import scipy.io

mat = scipy.io.loadmat("C:/Users/levon/Desktop/mK_saturation/levon_sm_hbt_2D_charac_qdac_2021_07_21_20_54_23.mat")
i_c = mat.get("i_c")*-1
i_b = mat.get("i_b")*-1
i_e=i_c+i_b
v_cb=mat.get("v_cb_full")
v_bc=-v_cb
v_eb=mat.get("v_eb_full")
v_be=-v_eb
v_ec=v_eb-v_cb

# i_e[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
# i_b[np.logical_or(i_b>50e-9, i_b<0)]=np.nan
mask=np.isnan(i_b)
v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan

Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
# Pwr[np.where(Pwr>20000*1e-9)]=np.nan


#%% design global parameters
cb_colour="viridis_r"
n_points=101
#%% 𝐼_E(𝑉_EC ,𝑉_BC), 𝐼_B(𝑉_EC ,𝑉_BC). 
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=v_ec*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[0].set_xlabel(r"$V_{EC}$ (mV)", size=20)
ax[1].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[1].set_xlabel(r"$V_{EC}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_B$ (nA)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝑉_𝐸𝐶 ,𝐼_𝐵), 𝑉_𝐵C(𝑉_𝐸𝐶 ,𝐼_𝐵).
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=i_b*1e9
Y=v_ec*1e03
Z1=i_e*1e9
Z2=v_bc*1e03
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_xlabel(r"$I_B$ (nA)", size=20)
ax[0].set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax[1].set_xlabel(r"$I_B$ (nA)", size=20)
ax[1].set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$V_{BC}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_E(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=i_e*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_E$ (nA)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% 𝐼_C(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=i_c*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_C$ (nA)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')

#%% g_m(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
Z=d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)*1e03
Z[np.where(Z<0)]=np.nan
Y=v_ec*1e03
X=i_b*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$g_m $ (mS)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% P(𝑉_EC,𝐼_B)
fig,ax = plt.subplots(figsize=(12,12))
X=i_b*1e09
Y=v_ec*1e03
Z=Pwr*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{EC}$ (mV)", size=20)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$P$ (nW)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝑉_BE ,𝑉_BC), 𝐼_B(𝑉_BE ,𝑉_BC). 
fig,ax = plt.subplots(1, 2, figsize=(16,8),facecolor="firebrick")
X=v_be*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[0].set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax[1].set_ylabel(r"$V_{BC}$ (mV)", size=20)
ax[1].set_xlabel(r"$V_{BE}$ (mV)", size=20)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading='auto')
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_E$ (nA)", size=20)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_B$ (nA)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[0].tick_params(width=1.5, length=6, which="minor",direction="out")
# ax[1].tick_params(width=1.5, length=12, which="major",direction="out")
# ax[1].tick_params(width=1.5, length=6, which="minor",direction="out")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
#%% 𝐼_𝐸(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$|I_E|$ (nA)", size=20)
ax.grid()
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=abs(i_e[n,:]*1e9)
    v=v_bc[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(v_bc)*1e3, vmax=np.max(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝐼_C(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$I_C$ (nA)", size=20)
ax.grid()
ax.tick_params(labelsize=16)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=i_c[n,:]*1e9
    v=v_bc[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(v_bc)*1e3, vmax=np.max(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#ax.set_ylabel(r"$\frac{dI_C}{dI_B}$", rotation=0, size=20)
ax.grid()
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_bc[n,0]*1e03
    x=i_b[n,:]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
# ax.set_yscale('log')
# ax.set_xlim(xmin=0)
# ax.set_ylim(ymin=0)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% diff 𝛽(𝐼_𝐵)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$I_B$ (nA)", size=20)
ax.set_ylabel(r"AC $\beta$", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_ylabel(r"$\frac{dI_C}{dI_B}$", rotation=0, size=20)
ax.grid()
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
AC_Beta[AC_Beta==np.nanmax(AC_Beta)]=np.nan
while n<= (n_points-1):
    v=v_bc[n,0]*1e03
    x=i_b[n,1:-2]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = AC_Beta[n,1:-2][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#ax.set_yscale('log')
#%% 𝛽(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
ax.set_xlabel(r"$P$ (nW)", size=20)
ax.set_ylabel(r"$\beta$", rotation=0, size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.2)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
#ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
Beta=i_c/i_b
# Beta[Beta==np.nanmax(Beta)]=np.nan
while n<= (n_points-1):
    v=v_bc[n,0]*1e03
    x=Pwr[n,]*1e9
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = Beta[n,:][indexs_to_order_by]
    mask=y_ordered<0
    y_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered,marker=".",lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(V_BE)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=20)
ax.set_xlabel(r"$V_{BE}$ (mV)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=20)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
ax.tick_params(labelsize=16)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=v_be[n,1:-2]*1e3
    v=v_bc[n,0]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered+offset, marker=".", lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")

#%% 𝑟_𝜋(𝐼_𝐵), 𝑟_𝑜(𝐼_𝐶)
fig,ax = plt.subplots(1, 2, figsize=(16,8))
ax[0].set_xlabel(r"$I_B$ (nA)", size=20)
ax[0].set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax[1].set_xlabel(r"$I_B$ (nA)", size=20)
ax[1].set_ylabel(r"$r_o$ $(\Omega)$", size=20)
ax[0].grid()
ax[1].grid()
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.4, hspace=0.2)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax[0].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax[1].set_prop_cycle(color=[cm(1.*i/(NUM_COLORS)) for i in range(NUM_COLORS)])
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
r_o=-np.gradient(v_ec,axis=0)/np.gradient(i_c,axis=0)
n=0
while n<= (n_points-1):
    v=v_bc[n,0]*1e03
    x1=i_b[n,]*1e9
    x2=i_b[n,:]*1e9
    indexs_to_order_by1 = x1.argsort()
    x_ordered1 = x1[indexs_to_order_by1]
    y_ordered1 = (r_pi[n,])[indexs_to_order_by1]
    mask=y_ordered1<0
    y_ordered1[mask]=np.nan
    ax[0].scatter(x_ordered1, y_ordered1, marker=".", lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    try: 
        y_ordered2 = (r_o[n,])[indexs_to_order_by2]
        mask=y_ordered2<0
        y_ordered2[mask]=np.nan
        ax[1].scatter(x_ordered2, y_ordered2, marker=".", lw=1)
        n+=1
    except: n+=1
# ax[0].legend(bbox_to_anchor=(0, -0.5),loc=3, ncol=7, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax[0].xaxis.set_major_formatter(formatter)
ax[1].xaxis.set_major_formatter(formatter)
ax[0].yaxis.set_major_formatter(formatter)
ax[1].yaxis.set_major_formatter(formatter)
sm1 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb1=plt.colorbar(sm1,ax=ax[0])
cb1.ax.set_title(r"$V_{BC}$ (mV)", size=20)
sm2 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc*1e3)))
cb2=plt.colorbar(sm2,ax=ax[1])
cb2.ax.set_title(r"$V_{BC}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
ax[0].set_yscale('log')
ax[1].set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax[0].spines[axis].set_linewidth(2)
  ax[1].spines[axis].set_linewidth(2)
ax[0].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[0].tick_params(width=1.5, length=12, which="major",direction="in")
ax[1].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[1].tick_params(width=1.5, length=12, which="major",direction="in")
#%% 𝑔_𝑚(Pwr)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=20)
ax.set_xlabel(r"$P$ (nW)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=20)
ax.set_ylabel(r"$g_m $ (mS)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(None, xmax=400)
# ax.set_ylim([-0.5,0.5])
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=Pwr[n,1:-2]*1e9
    v=v_bc[n,0]*1e3
    X=x
    indexs_to_order_BY = X.argsort()
    X_ordered = X[indexs_to_order_BY]
    Y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_BY]*1e03
    mask=Y_ordered<0
    Y_ordered[mask]=np.nan
    ax.scatter(X_ordered, Y_ordered+offset, marker=".", lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
# fig2, ax2 = plt.subplots(figsize=(8, 8),facecolor="firebrick")
# norm = mpl.colors.Normalize(𝑔__𝑚.min(), 𝑔__𝑚.max())
# p = ax2.pcolor(i_b*1e9, v_ec*1e3, 𝑔__𝑚, norm=norm, cmap='inferno', shading='nearest')
# ax2.set_xlabel(r"$I_B$ $(nA)$")
# ax2.set_ylabel(r"$V_{EC}$ $(mV)$")
# cb = fig.colorbar(p, ax=ax2)
# cb.ax.set_title(r"$g_m$ $(nS)$")
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
# ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑔_𝑚(i_b)
fig,ax = plt.subplots(figsize=(12,12))
# ax.set_facecolor("dimgray")
#ax.set_xlabel(r"$I_B$ $(nA)$", size=18)
ax.set_xlabel(r"$I_B$ (nA)", size=20)
#ax.set_ylabel(r"$\frac{dI_C}{dV_{BE}}$ $(nS)$", rotation=0, size=18)
ax.set_ylabel(r"$g_m $ (S)", size=20)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.yaxis.set_major_formatter(formatter)
#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.2, hspace=0.5)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
offset= 0 #(mS)
d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
n=0
while n<= (n_points-1):
    x=i_b[n,1:-2]*1e9
    v=v_bc[n,0]*1e3
    X=x
    indexs_to_order_by = X.argsort()
    x_ordered = X[indexs_to_order_by]
    y_ordered = (d𝑔_𝑚[n,1:-2])[indexs_to_order_by]
    mask=y_ordered<0
    x_ordered[mask]=np.nan
    ax.scatter(x_ordered, y_ordered+offset, marker=".", lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    n+=1
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_{BC}$ (mV)", size=20)
cb.ax.tick_params(labelsize=12)
ax.tick_params(labelsize=16)
ax.set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#%% 𝑟_𝜋(Pwr), 𝑟_𝑜(Pwr)
fig,ax = plt.subplots(1, 2, figsize=(16,8))
ax[0].set_xlabel(r"$P$ (nW)", size=20)
ax[0].set_ylabel(r"$r_\pi$ $(\Omega)$", size=20)
ax[1].set_xlabel(r"$P$ (nW)", size=20)
ax[1].set_ylabel(r"$r_o$ $(\Omega)$", size=20)
ax[0].grid()
ax[1].grid()
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
#wspace=0.4, hspace=0.2)
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax[0].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax[1].set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)
np.seterr(divide='ignore', invalid='ignore')
r_o=-np.gradient(v_ec,axis=0)/np.gradient(i_c,axis=0)
n=0
while n<= (n_points-1):
    v=v_bc[n,0]*1e03
    x1=Pwr[n,:]*1e9
    x2=Pwr[n,:]*1e9
    indexs_to_order_by1 = x1.argsort()
    x_ordered1 = x1[indexs_to_order_by1]
    y_ordered1 = (r_pi[n,])[indexs_to_order_by1]
    mask=y_ordered1<0
    y_ordered1[mask]=np.nan
    ax[0].scatter(x_ordered1, y_ordered1, marker=".", lw=1, label=r"$V_{BC}$=%.1f (mV)" % v)
    indexs_to_order_by2 = x2.argsort()
    x_ordered2 = x2[indexs_to_order_by2]
    try: 
        y_ordered2 = (r_o[n,])[indexs_to_order_by2]
        mask=y_ordered2<0
        y_ordered2[mask]=np.nan
        ax[1].scatter(x_ordered2, y_ordered2, marker=".", lw=1)
        n+=1
    except: n+=1
# ax[0].legend(bbox_to_anchor=(0, -0.5),loc=3, ncol=7, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax[0].xaxis.set_major_formatter(formatter)
ax[1].xaxis.set_major_formatter(formatter)
ax[0].yaxis.set_major_formatter(formatter)
ax[1].yaxis.set_major_formatter(formatter)
sm1 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc)*1e3))
cb1=plt.colorbar(sm1,ax=ax[0])
cb1.ax.set_title(r"$V_{BC}$ (mV)", size=20)
sm2 = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.nanmin(v_bc)*1e3, vmax=np.nanmax(v_bc*1e3)))
cb2=plt.colorbar(sm2,ax=ax[1])
cb2.ax.set_title(r"$V_{BC}$ (mV)", size=20)
ax[0].tick_params(labelsize=16)
ax[1].tick_params(labelsize=16)
cb1.ax.tick_params(labelsize=12)
cb2.ax.tick_params(labelsize=12)
ax[0].set_yscale('log')
ax[1].set_yscale('log')
for axis in ['top','bottom','left','right']:
  ax[0].spines[axis].set_linewidth(2)
  ax[1].spines[axis].set_linewidth(2)
ax[0].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[0].tick_params(width=1.5, length=12, which="major",direction="in")
ax[1].tick_params(width=1.5, length=6, which="minor",direction="in")
ax[1].tick_params(width=1.5, length=12, which="major",direction="in")
#%% g_m(𝑉_BC, 𝑉_BE,)
fig,ax = plt.subplots(figsize=(12,12))
Z=d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)*1e03
Z[np.where(Z<0)]=np.nan
Y=v_be*1e03
X=v_bc*1e03
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_{BE}$ (mV)", size=20)
ax.set_xlabel(r"$V_{BC}$ (mV)", size=20)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$g_m $ (mS)", size=20)
ax.tick_params(labelsize=16)
cb.ax.tick_params(labelsize=12)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=1.5, length=12, which="major",direction="out")
# ax.tick_params(width=1.5, length=6, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')