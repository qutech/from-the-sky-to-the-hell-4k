# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 16:59:58 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
os.chdir("C:/Users/levon/Desktop/saturation_noise_plots")

mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_sat"
p=os.listdir(mypath)
adj=p[-1]
p1=p[0:10]
p1.append(adj)
del adj
p2=p[10:-1]
p=p1+p2
n=len(p)
print("Please choose the experiment number ranging from 0 to {0:.0f}".format(n-1) )
k = int( input() )
#%%
fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.set_xlabel(r"f (Hz)", size=20)
#ax.set_title("Noise spectral density of collecotor curren for different emmiter voltages", fontsize=20, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
#ax.text(0, 0.1, "Text label", fontsize=14, family="serif")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
# NUM_COLORS = n
# cm = plt.get_cmap("inferno")
# ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_sat/{0:s}".format( p[k] ) )
gain=(mat.get("userdata")["amp_gain"])[0,0].copy()
gain=gain[0,0]
gain=float(gain)
FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
FFT_data=FFT_data[:,0]
FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
FFT_freq=FFT_freq[:,0]
timestep=(mat.get("spectra")["timestep"])[0,0].copy()
timestep=timestep[0,0]
hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
hardwareSampFreq=hardwareSampFreq[0,0]
configvals=(mat.get("userdata")["configvals"])[0,0].copy()
v_eb_noise=configvals[0,23]*1000
v_eb_noise=v_eb_noise[0,0]
v_cb_noise=configvals[0,22]*1000
v_cb_noise=v_cb_noise[0,0]
v_bc_noise=-v_cb_noise
v_ec_noise=v_eb_noise-v_cb_noise
# FFT_data[np.where(FFT_freq>=500000)]=np.nan
# FFT_data = FFT_data[~np.isnan(FFT_data)]
# FFT_freq[np.where(FFT_freq>=500000)]=np.nan
# FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
N=FFT_freq.size
f_s=hardwareSampFreq #sampling frquency
delta_f=f_s/N #resolution of the frequency spectrum
B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
f=FFT_freq
T=N/f_s
S_omega_i_c=FFT_data
ax.plot(f,np.sqrt(S_omega_i_c/(gain**2)), color="olivedrab", lw=0.3, label=r"$V_{{EC}}$={0:.1f} (mV), $V_{{BC}}$={1:.1f} (mV)".format(v_ec_noise, -1*v_cb_noise) )

mat = scipy.io.loadmat("C:/Users/levon/Desktop/mK_saturation/levon_sm_hbt_2D_charac_qdac_2021_07_21_20_54_23.mat")
i_c = mat.get("i_c").T*-1.
i_b = mat.get("i_b").T*-1
i_e=i_c+i_b
v_cb=mat.get("v_cb_full").T
v_bc=-v_cb
v_eb=mat.get("v_eb_full").T
v_be=-v_eb
v_ec=v_eb-v_cb

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
r_pi=np.gradient(v_be,axis=1)/np.gradient(i_b,axis=1)


for j in np.arange(0, np.size(v_ec[0,:])):
    for i in np.arange(0, np.size(v_ec[:,0])):
        if ( (v_ec[i,j]*1000==round(v_ec_noise,1)) and (v_bc[i,j]*1000==round(v_bc_noise,1) ) ):
            break
        else: continue

delta_i_c=( np.gradient(i_c,axis=1) )[i,j]

i_b=i_b[i,j]*np.ones(N)
i_c=i_c[i,j]*np.ones(N)
#v_be=v_be[0,i]*np.ones(N)
g_m=g_m[i,j]*np.ones(N)
r_pi=r_pi[i,j]*np.ones(N)
r_set=2e5
R=r_set*r_pi/(r_set+r_pi)

F=1
S_b_shot= np.sqrt(2*e*i_b*F)
S_b_shot_amp=S_b_shot*g_m*R
ax.plot(f,S_b_shot_amp, lw=1, color="maroon", label="Base amplifed shot noise")

S_set_shot_amp=np.sqrt(2*e*F*delta_i_c*g_m*R)
ax.plot(f,S_set_shot_amp, lw=1, color="navy", label="SET amplifed shot noise")

S_c_shot=np.sqrt(2*e*i_c*F)
ax.plot(f, S_c_shot,'red', lw=1, label="Collector shot noise")

S_total=S_b_shot_amp**2+S_set_shot_amp**2+S_c_shot**2
ax.plot(f, np.sqrt(S_total),'darkorange', lw=3, label="Total output shot noise")
#ax.plot(f,np.sqrt(S_omega_i_c/(gain**2)-S_total), lw=0.3, label=r"$V_{{EC}}$={0:.1f} (mV), $V_{{BC}}$={1:.1f} (mV)".format(v_ec_noise, -1*v_cb_noise))

xcoords = [500e3, 580e3]
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")

leg=ax.legend(loc=3, borderaxespad=0.0)
for line in leg.get_lines():
    line.set_linewidth(2)
plt.text(0.75, 0.85, "amplifier cutoff", rotation=90, transform=ax.transAxes, verticalalignment='center', size=20, family="serif", color="maroon")

print("Do you want to save the plot?")
answer = input()
if answer == "1":
        plt.savefig("Sat_mK_individualpoint{0:.0f}.pdf".format(k)) 
        plt.savefig("Sat_mK_individualpoint{0:.0f}.png".format(k))
if answer == "0": pass
