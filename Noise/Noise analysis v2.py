# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 10:06:43 2021

@author: levon
"""
import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt

import seaborn as sns
import math
from pprint import pprint
from scipy.constants import elementary_charge as e
 
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D4/4K with IV converter- Saturation mode measurments/03-06-2021/#013_18-08-08_Base Sweep"

dataset=qc.data.data_set.load_data(p)
print(dataset.read)

g1=dataset.metadata.get("station").get("parameters").get("Collector_amplification_of_IV_converter").get("value")
g2=dataset.metadata.get("station").get("parameters").get("Base_amplification_of_IV_converter").get("value")
v_ec = dataset.qdac_chan01_v.ndarray.copy()
v_bc = dataset.qdac_chan02_v.ndarray.copy()
i_c = dataset.dmm1_volt.ndarray.copy()*1/g1*-1
i_b = dataset.dmm2_volt.ndarray.copy()*1/g2*-1
i_e=i_c+i_b

# i_c[np.logical_or(i_b>1.1e-06, i_b<0)]=np.nan
# i_b[np.logical_or(i_b>1.1e-06, i_b<0)]=np.nan 
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)

v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_e[mask]=np.nan

v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
𝑔_𝑚=np.gradient(i_c)/np.gradient(v_be)
r_pi=np.gradient(v_be)/np.gradient(i_b)
r_set=2e5
R=r_set*r_pi/(r_set+r_pi)
F=1
S_b_shot= np.sqrt(2*e*i_b*F)
S_b_shot_amp=S_b_shot*g_m*R

fig,ax = plt.subplots(1, 1, figsize=(12,12))
ax.set_xlabel("Power (nW)", size=20)
ax.set_ylabel(r"Noise (pA/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.grid()
ax.scatter(Pwr*1e9,S_b_shot*1e12, marker=".", lw=1, color="navy", label="Base shot noise")
ax.scatter(Pwr*1e9,S_b_shot_amp*1e12, marker=".", lw=1, color="maroon", label="Collector shot noise")
ax.legend(loc=4, borderaxespad=0.0)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
plt.savefig("my_shot_noise_test.pdf")