# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 11:00:26 2020

@author: Baghumyan
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
import scipy.io

#%% Tradition QCoDeS way
#p = "C:/Users/Baghumyan/Desktop/D_04_20_01/30-11-2020/30-11-2020 at 15_54_50 o'clock.db"
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D1/4K with grounding cable, scenario2/18-01-2021/#004_12-13-56_2D_Sweep, s2/18-01-2021 at 13_01_34 o_clock.db"
initialise_or_create_database_at(p)
from qcodes.dataset.experiment_container import experiments
print(experiments())
time.sleep(0.5)
id=input("Please enter the experiment run id that you wish to use for noise calculations \nid=")
dataset=qc.dataset.data_set.load_by_id(id)
print("Captured run id: {}".format(dataset.captured_run_id))
print("Sample name: {}".format(dataset.sample_name))
print("Experiment name: {}".format(dataset.exp_name))
print(dataset.get_parameter_data)
t_general=dataset.get_parameter_data("Time").get("Time").get("Time")
v_ec = dataset.get_parameter_data("keithley1_source_voltage").get("keithley1_source_voltage").get("keithley1_source_voltage")
v_bc = dataset.get_parameter_data("keithley2_source_voltage").get("keithley2_source_voltage").get("keithley2_source_voltage")
i_e = dataset.get_parameter_data("keithley1_sense_current").get("keithley1_sense_current").get("keithley1_sense_current")
i_b = dataset.get_parameter_data("keithley2_sense_current").get("keithley2_sense_current").get("keithley2_sense_current")
i_c=((-1*i_e)-i_b)
v_be=v_bc-v_ec
#%%New numpy way
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D1/4K with grounding cable, scenario2/18-01-2021/noise_result1 with cable4.npy"
data=np.array(np.load(p),dtype=np.float32)
t=data[:,0]
v_bc=data[:,1]
i_b=data[:,2]
#%% Base current noise spectral density
#Note, that I use Prof. Bluhm's convention for frequency integration and consequently his noise constant
Beta=i_b
Beta_name="keithley2_sense_current"
try: t=dataset.get_parameter_data(Beta_name).get(Beta_name).get("Time")
except: print("using the scenario #2")
N=Beta.size
T=t[-1]
f_s=N/T #sampling frquency
delta_f=f_s/N #resolution of the frequency spectrum
B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
ftsum=np.zeros(N)
f=fftpack.fftfreq(N,1/f_s)
#mask=np.where(f>=0)
window=signal.blackman(N)
ftsum=ftsum+abs(fftpack.fft(Beta*window))**2
S_omega_b=ftsum/T
fig,ax = plt.subplots(1, 1, figsize=(12,12))
# mask=np.where(f>=0) 
ax.plot(f,S_omega_b,'.r')
#ax.set_title("Noise spectral density of the base current", fontsize=14, fontname='serif', color="blue")
ax.set_ylabel(r"Noise (pA/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.set_xlabel(r"$\mathrm{\Omega} $ (Hz)", size=20)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
Z_o=50
T_N=S_omega_b*Z_o/k
print("Noise temperature is {} Kelvin".format(T_N))
AC_gain=np.gradient(i_c)/np.gradient(i_b)
#%% Collector current noise spectral density
#Note, that I use Prof. Bluhm's convention for frequency integration and consequently his noise constant
Beta=i_c
Beta_name="keithley2_sense_current"
t=dataset.get_parameter_data(Beta_name).get(Beta_name).get("Time")
N=Beta.size
T=t[-1]
f_s=N/T #sampling frquency
delta_f=f_s/N #resolution of the frequency spectrum
B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
ftsum=np.zeros(N)
f=fftpack.fftfreq(N,1/f_s)
#mask=np.where(f>=0)
window=signal.blackman(N)
ftsum=ftsum+abs(fftpack.fft(Beta*window))**2
S_omega_c=ftsum/T
fig,ax = plt.subplots(1, 1, figsize=(12,12))
# mask=np.where(f>=0) 
# ax[0].plot(f[mask],S_omega[mask],'og')
ax[0].plot(f,S_omega_c,'.g')
ax.set_ylabel(r"Noise (pA/$\mathrm{\sqrt{Hz}}$)", size=20)
ax.set_xlabel(r"$\mathrm{\Omega} $ (Hz)", size=20)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
ax.xaxis.set_major_formatter(formatter)
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=1.5, length=12, which="major",direction="in")
ax.tick_params(width=1.5, length=6, which="minor",direction="in")
#NOTE THAT THE NEGATIVE Ω VALUES ARE DISREGARDED IN THE SECOND PLOT BECAUSE OF LOG SCALE
Z_o=50
T_N=S_omega_c*Z_o/k
print("Noise temperature is {} Kelvin".format(T_N))
#%%
AC_gain=np.gradient(i_c)/np.gradient(i_b)
nf=20*np.log((S_omega_c/AC_gain)/S_omega_b)
print("Noise fugure is {}".format(nf))