# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 12:31:46 2021

@author: levon
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 14:56:03 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
os.chdir("C:/Users/levon/Desktop/hbt_noise")

mypath="C:/Users/levon/Desktop/hbt_noise/sat_regime_2ndivc_left"
p=os.listdir(mypath)
# p=[ p[-1], p[0], p[-2], p[1], p[-3], p[2], p[3], p[4], p[5] ]
n=len(p)


#%%

for i in range(n):
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/hbt_noise//sat_regime_2ndivc_left/{0:s}".format( p[i] ) )
    ( (mat.get("spectra")["data"])[0,0] )[0,0]= [0]
    scipy.io.savemat(p[i], mat)