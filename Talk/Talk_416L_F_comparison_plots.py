# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 19:52:23 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#%matplotlib inline
#%matplotlib auto
#import seaborn as sns
import math

#%% design global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 - 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 72.27*10
n_points=51
size=24
labelsize=20
point_size=30
tick_width=1.5
#%%
p1 = "C:/Users/levon/Desktop/DATA/HBT/416L-L2-D3/4K with grounding cable/08-02-2021/#008_15-42-10_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p2 = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D1/4K with grounding cable/03-02-2021/#013_16-10-05_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p3 = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D3/4K with grounding cable/01-03-2021/#006_16-04-47_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p4 = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D4/4K with grounding cable/18-02-2021/#005_17-56-58_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p5 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D1/4K with grounding cable/09-02-2021/#012_11-55-03_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p6 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D2/4K with grounding cable/09-02-2021/#004_2D_Sweep_12-50-27/keithley1_source_voltage_set_keithley2_source_voltage_set"
p7 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D3/4K with grounding cable/09-02-2021/#006_14-08-53_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p8 = "C:/Users/levon/Desktop/DATA/HBT/416L-L6-D4/4K with grounding cable/09-02-2021/#007_14-48-55_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p9 = "C:/Users/levon/Desktop/DATA/HBT/416L-L7-D1/4K with grounding cable/15-02-2021/#006_13-48-55_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p10 = "C:/Users/levon/Desktop/DATA/HBT/416L-L7-D2/4K with grounding cable/15-02-2021/#019_15-21-47_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p11 = "C:/Users/levon/Desktop/DATA/HBT/416L-L7-D3/4K with grounding cable/15-02-2021/#006_16-19-10_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
p12 = "C:/Users/levon/Desktop/DATA/HBT/416L-L8-D1/4K with grounding cable/10-02-2021/#006_11-18-27_2D_Sweep/keithley1_source_voltage_set_keithley2_source_voltage_set"
os.chdir("C:/Users/levon/Desktop/talk_plots/416L_F_comarison")

fig1,ax1 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax1.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax1.set_ylabel(r"$\beta$", rotation=0, size=size)
ax1.grid()
# ax1.set_yscale('log')
fig2,ax2 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax2.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax2.set_ylabel(r"AC $\beta$", size=size)
ax2.grid()
fig3,ax3 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax3.set_xlabel(r"$V_\mathrm{BE}$ (mV)", size=size)
ax3.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax3.grid()
# ax3.set_xlim(900, xmax=np.max(v_be)*1e3)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
ax3.yaxis.set_major_formatter(formatter)
ax3.set_yscale('log')
fig4,ax4 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax4.set_xlabel(r"$P$ (nW)", size=size)
ax4.set_ylabel(r"$g_\mathrm{m} $ (mS)", size=size)
ax4.yaxis.set_major_formatter(formatter)
ax4.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.grid()
fig5,ax5 = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax5.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax5.set_ylabel(r"$g_\mathrm{m} $ (S)", size=size)
ax5.grid()
ax5.yaxis.set_major_formatter(formatter)
ax5.set_yscale('log')
p=[p1,p2,p3,p4, p5, p6, p7, p8, p9, p10, p11, p12]
safe= ["#88CCEE","#CC6677","#DDCC77","#117733","#332288","#AA4499","#44AA99","#999933","#882255","#661100","#6699CC","#888888"]
ax1.set_prop_cycle(color=safe)
ax2.set_prop_cycle(color=safe)
ax3.set_prop_cycle(color=safe)
ax4.set_prop_cycle(color=safe)
ax5.set_prop_cycle(color=safe)
#cm = plt.get_cmap("magma")
# ax1.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
# ax2.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
# ax3.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
# ax4.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
# ax5.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for k in p:
    dataset=qc.data.data_set.load_data(k)
    print(dataset.read)
    v_ec = dataset.keithley1_source_voltage.ndarray.copy()
    v_bc = dataset.keithley2_source_voltage.ndarray.copy()
    i_e = dataset.keithley1_sense_current.ndarray.copy()
    i_b = dataset.keithley2_sense_current.ndarray.copy()
    v_be=v_bc-v_ec
    i_c=((-1*i_e)-i_b) #or directly measure it
    Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)
    Pwr[Pwr<0]=np.nan
    i_e[np.logical_or(i_b>5e-9, i_b<0)]=np.nan
    i_b[np.logical_or(i_b>5e-9, i_b<0)]=np.nan
    mask=np.isnan(i_b)
    v_ec[mask]=np.nan
    v_bc[mask]=np.nan
    v_be[mask]=np.nan
    i_b[mask]=np.nan
    i_c[mask]=np.nan

    Beta=i_c/i_b
    if np.nanmax(Beta)>2000:
        v_be[Beta==np.nanmax(Beta)]=np.nan
        Beta[Beta==np.nanmax(Beta)]=np.nan
    X1=i_b*1e9
    # indexs_to_order_by = X1.argsort()
    # x_ordered = X1[indexs_to_order_by]
    # y_ordered = Beta[indexs_to_order_by]
    # mask1=y_ordered<0
    # y_ordered[mask1]=np.nan
    Sampname=k[32:42]
    ax1.scatter(X1, Beta,marker=".", s=point_size, label=Sampname)
    AC_Beta=np.gradient(i_c,axis=1)/np.gradient(i_b,axis=1)
    AC_Beta[AC_Beta<0]=np.nan
    AC_Beta[AC_Beta>2000]=np.nan
    if np.nanmax(AC_Beta)>2000:
        AC_Beta[AC_Beta==np.nanmax(AC_Beta)]=np.nan
    X2=X1
    ax2.scatter(X2, AC_Beta, marker=".", s=point_size, label=Sampname)
    d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
    X3=v_be*1e3
    # Indexs_to_order_by = X.argsort()
    # X_ordered = X[Indexs_to_order_by]
    # Y_ordered = (d𝑔_𝑚)[Indexs_to_order_by]
    mask2=d𝑔_𝑚<0
    d𝑔_𝑚[mask2]=np.nan
    # if np.nanmax(d𝑔_𝑚)>0.3e-3:
    #     d𝑔_𝑚[d𝑔_𝑚==np.nanmax(d𝑔_𝑚)]=np.nan
    ax3.scatter(X3[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", s=point_size, label=Sampname)
    X4=Pwr*1e9
    ax4.scatter(X4[:,1:-2], d𝑔_𝑚[:,1:-2]*1e3, marker=".", s=point_size, label=Sampname)
    X5=i_b*1e9
    ax5.scatter(X5[:,1:-2], d𝑔_𝑚[:,1:-2], marker=".", s=point_size, label=Sampname)
leg1=ax1.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize})
for line in leg1.get_lines():
    line.set_linewidth(2)
for handle in leg1.legendHandles:
    handle.set_sizes([120.0])
leg2=ax2.legend(loc=1, borderaxespad=0.0, prop={'size': labelsize})
for line in leg1.get_lines():
    line.set_linewidth(2)
for handle in leg2.legendHandles:
    handle.set_sizes([120.0])
leg3=ax3.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize})
for line in leg3.get_lines():
    line.set_linewidth(2)
for handle in leg3.legendHandles:
    handle.set_sizes([120.0])
leg4=ax4.legend(loc=2, borderaxespad=0.0, prop={'size': labelsize})
for line in leg4.get_lines():
    line.set_linewidth(2)
for handle in leg4.legendHandles:
    handle.set_sizes([120.0])
    leg5=ax5.legend(loc=4, borderaxespad=0.0, prop={'size': labelsize})
for line in leg5.get_lines():
    line.set_linewidth(2)
for handle in leg5.legendHandles:
    handle.set_sizes([120.0])
ax1.tick_params(labelsize=labelsize)
ax2.tick_params(labelsize=labelsize)
ax3.tick_params(labelsize=labelsize)
ax4.tick_params(labelsize=labelsize)
ax5.tick_params(labelsize=labelsize)
ax1.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax1.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax2.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax3.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax4.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax5.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax5.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax1.spines[axis].set_linewidth(2)
  ax2.spines[axis].set_linewidth(2)
  ax3.spines[axis].set_linewidth(2)
  ax4.spines[axis].set_linewidth(2)
  ax5.spines[axis].set_linewidth(2)
ax1.tick_params(width=tick_width, length=12, which="major",direction="in")
ax1.tick_params(width=tick_width, length=6, which="minor",direction="in")
ax2.tick_params(width=tick_width, length=12, which="major",direction="in")
ax2.tick_params(width=tick_width, length=6, which="minor",direction="in")
ax3.tick_params(width=tick_width, length=12, which="major",direction="in")
ax3.tick_params(width=tick_width, length=6, which="minor",direction="in")
ax4.tick_params(width=tick_width, length=12, which="major",direction="in")
ax4.tick_params(width=tick_width, length=6, which="minor",direction="in")
ax5.tick_params(width=tick_width, length=12, which="major",direction="in")
ax5.tick_params(width=tick_width, length=6, which="minor",direction="in")

ax4.set_xlim(xmax=1000)
ax4.set_ylim(ymax=0.2)
fig1.savefig("416L_F_comparison1.pdf", bbox_inches='tight') 
fig1.savefig("416L_F_comparison1.png", bbox_inches='tight')
fig2.savefig("416L_F_comparison2.pdf", bbox_inches='tight') 
fig2.savefig("416L_F_comparison2.png", bbox_inches='tight')
fig3.savefig("416L_F_comparison3.pdf", bbox_inches='tight') 
fig3.savefig("416L_F_comparison3.png", bbox_inches='tight')
fig4.savefig("416L_F_comparison4.pdf", bbox_inches='tight') 
fig4.savefig("416L_F_comparison4.png", bbox_inches='tight')
fig5.savefig("416L_F_comparison5.pdf", bbox_inches='tight') 
fig5.savefig("416L_F_comparison5.png", bbox_inches='tight')

    
