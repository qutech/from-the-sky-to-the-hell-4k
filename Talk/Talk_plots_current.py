# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 18:43:15 2021

@author: levon
"""

import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
#%matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.linear_model import LinearRegression
 
p = "C:/Users/levon/Desktop/DATA/HBT/416L-L5-D2/4K with current sourcing- Saturation mode measurments/06-18-2021/#002_2D_Sweep with current sourcing_19-37-29"
os.chdir("C:/Users/levon/Desktop/talk_plots/416L-L5-D2/saturation")
Sampname=p[32:42]
dataset=qc.data.data_set.load_data(p)
print(dataset.read)
#dataset.default_parameter_name()
#dataset.default_parameter_array()
#dataset.read
# A=dataset.default_parameter_array()
# J=dataset.keithley1_sense_current.ndarray
# C=A.ndarray
v_ec = dataset.keithley1_source_voltage.ndarray.copy()
v_bc = dataset.keithley2_sense_voltage.ndarray.copy()
i_e = dataset.keithley1_sense_current.ndarray.copy()
i_b = dataset.keithley2_source_current.ndarray.copy()

i_c=i_e*-1-i_b

i_e[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
i_b[np.logical_or(i_b>10e-9, i_b<0)]=np.nan
# i_b[i_e==np.nanmin(i_e)]=np.nan
# i_e[i_e==np.nanmin(i_e)]=np.nan
mask=np.isnan(i_b)

# i_b[np.abs(i_e)>40000e-9]=np.nan
# i_e[np.abs(i_e)>40000e-9]=np.nan
# mask=np.isnan(i_e)
# v_ec[mask]=np.nan
# v_bc[mask]=np.nan

# v_bc[v_bc>0]=np.nan
# mask=np.isnan(v_bc)


v_ec[mask]=np.nan
v_bc[mask]=np.nan
i_b[mask]=np.nan
i_c[mask]=np.nan


v_be=v_bc-v_ec
Pwr=np.abs(i_c*v_ec)+np.abs(i_b*v_be)


# Pwr[np.where(Pwr>20000*1e-9)]=np.nan
# Beta[Beta==np.nanmax(Beta)]=np.nan

# mask=np.logical_and(i_b>=0.1e-9, i_b<=10e-9)
# A1=v_ec[mask]
# B1=v_bc[mask]
# A2=v_ec[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=v_be[(np.where((i_b>=0.1e-9) & (i_b<=10e-9)))]
# B2=np.sort(B2)

#%% design global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 - 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 72.27*10
cb_colour="viridis_r"
n_points=51
size=24
labelsize=22
point_size=30
tick_width=1.5
#%% I_E(V_EC, V_BC), I_B(V_EC, V_BC)
fig,ax = plt.subplots(1, 2, figsize=set_size(latex_width, fraction=1, subplots=(1, 2)))
X=v_ec*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_\mathrm{BC}$ (mV)", size=size)
ax[0].set_xlabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax[1].set_ylabel(r"$V_\mathrm{BC}$ (mV)", size=size)
ax[1].set_xlabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_\mathrm{E}$ (nA)", size=size)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_\mathrm{B}$ (nA)", size=size)
cb1.ax.tick_params(labelsize=labelsize)
cb2.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[0].tick_params(width=tick_width, length=7, which="major",direction="in")
# ax[1].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[1].tick_params(width=tick_width, length=7, which="major",direction="in")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
ax[0].tick_params(labelsize=labelsize)
ax[1].tick_params(labelsize=labelsize)
plt.tight_layout()
plt.savefig(Sampname+"_I_E(V_EC ,V_BC)_and_I_B(V_EC ,V_BC).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_E(V_EC ,V_BC)_and_I_B(V_EC ,V_BC).png", bbox_inches='tight')
#%% I_E(V_EC, I_B)_and_V_BC(V_EC, I_B).
fig,ax = plt.subplots(1, 2, figsize=set_size(latex_width, fraction=1, subplots=(1, 2)))
X=i_b*1e9
Y=v_ec*1e03
Z1=i_e*1e9
Z2=v_bc*1e03
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax[0].set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax[1].set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax[1].set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading="auto")
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_\mathrm{E}$ (nA)", size=size)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$V_\mathrm{BC}$ (mV)", size=size)
cb1.ax.tick_params(labelsize=labelsize)
cb2.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[0].tick_params(width=tick_width, length=7, which="major",direction="in")
# ax[1].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[1].tick_params(width=tick_width, length=7, which="major",direction="in")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
ax[0].tick_params(labelsize=labelsize)
ax[1].tick_params(labelsize=labelsize)
plt.tight_layout()
plt.savefig(Sampname+"_I_E(V_EC, I_B)_and_V_BC(V_EC, I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_E(V_EC, I_B)_and_V_BC(V_EC, I_B).png", bbox_inches='tight')
#%% I_E(V_EC, I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
X=i_b*1e09
Y=v_ec*1e03
Z=i_e*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_\mathrm{E}$ (nA)", size=size)
cb.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=tick_width, length=7, which="major",direction="out")
# ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_I_E(V_EC, I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_E(V_EC, I_B).png", bbox_inches='tight')
#%% I_C(V_EC, I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
X=i_b*1e09
Y=v_ec*1e03
Z=i_c*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$I_\mathrm{C}$ (nA)", size=size)
cb.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=tick_width, length=7, which="major",direction="out")
# ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_I_C(V_EC, I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_C(V_EC, I_B).png", bbox_inches='tight')
#%% g_m(V_EC, I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
Z=d𝑔_𝑚=np.gradient(i_c,axis=1)/np.gradient(v_be,axis=1)
Z[np.where(Z<0)]=np.nan
Y=v_ec*1e03
X=i_b*1e9
norm = mpl.colors.LogNorm(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$g_\mathrm{m} $ (S)", size=size)
cb.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=tick_width, length=7, which="major",direction="out")
# ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_g_m(V_EC, I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_g_m(V_EC, I_B).png", bbox_inches='tight')
#%% P(V_EC, I_B)
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
X=i_b*1e09
Y=v_ec*1e03
Z=Pwr*1e9
norm = mpl.colors.Normalize(np.nanmin(Z),np.nanmax(Z))
ax.set_xlim(np.nanmin(X),np.nanmax(X))
ax.set_ylim(np.nanmin(Y),np.nanmax(Y))
ax.set_ylabel(r"$V_\mathrm{EC}$ (mV)", size=size)
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
p = ax.pcolor(X, Y, Z, norm=norm, cmap='inferno', shading="auto")
cb = fig.colorbar(p, ax=ax)
cb.ax.set_title(r"$P$ (nW)", size=size)
cb.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax.spines[axis].set_linewidth(2)
# ax.tick_params(width=tick_width, length=7, which="major",direction="out")
# ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.xaxis.set_ticks_position('both')
ax.yaxis.set_ticks_position('both')
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_P(V_EC, I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_P(V_EC, I_B).png", bbox_inches='tight')
#%% I_E(V_BE, V_BC), I_B(V_BE, V_BC). 
fig,ax = plt.subplots(1, 2, figsize=set_size(latex_width, fraction=1, subplots=(1, 2)))
X=v_be*1e03
Y=v_bc*1e03
Z1=i_e*1e9
Z2=i_b*1e9
norm1 = mpl.colors.Normalize(np.nanmin(Z1),np.nanmax(Z1))
norm2 = mpl.colors.Normalize(np.nanmin(Z2),np.nanmax(Z2))
ax[0].set_xlim(np.nanmin(X),np.nanmax(X))
ax[0].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[1].set_xlim(np.nanmin(X),np.nanmax(X))
ax[1].set_ylim(np.nanmin(Y),np.nanmax(Y))
ax[0].set_ylabel(r"$V_\mathrm{BC}$ (mV)", size=size)
ax[0].set_xlabel(r"$V_\mathrm{BE}$ (mV)", size=size)
ax[1].set_ylabel(r"$V_\mathrm{BC}$ (mV)", size=size)
ax[1].set_xlabel(r"$V_\mathrm{BE}$ (mV)", size=size)
ax[0].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[0].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax[1].yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax[0].set_xlim(xmin=900, xmax=965)
# ax[1].set_xlim(xmin=900, xmax=965)
# ax[0].set_ylim(ymin=-75, ymax=890)
# ax[1].set_ylim(ymin=-75, ymax=890)
p1 = ax[0].pcolor(X, Y, Z1, norm=norm1, cmap='inferno', shading='auto')
p2 = ax[1].pcolor(X, Y, Z2, norm=norm2, cmap='inferno', shading='auto')
cb1 = fig.colorbar(p1, ax=ax[0])
cb1.ax.set_title(r"$I_\mathrm{E}$ (nA)", size=size)
cb2 = fig.colorbar(p2, ax=ax[1])
cb2.ax.set_title(r"$I_\mathrm{B}$ (nA)", size=size)
cb1.ax.tick_params(labelsize=labelsize)
cb2.ax.tick_params(labelsize=labelsize)
# plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95,
# wspace=0.4, hspace=0.2)
formatter = mpl.ticker.ScalarFormatter(useMathText=True)
cb1.ax.yaxis.set_major_formatter(formatter)
cb2.ax.yaxis.set_major_formatter(formatter)
# for axis in ['top','bottom','left','right']:
#   ax[0].spines[axis].set_linewidth(2)
#   ax[1].spines[axis].set_linewidth(2)
# ax[0].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[0].tick_params(width=tick_width, length=7, which="major",direction="in")
# ax[1].tick_params(width=tick_width, length=4, which="minor",direction="in")
# ax[1].tick_params(width=tick_width, length=7, which="major",direction="in")
ax[0].xaxis.set_ticks_position('both')
ax[0].yaxis.set_ticks_position('both')
ax[1].xaxis.set_ticks_position('both')
ax[1].yaxis.set_ticks_position('both')
ax[0].tick_params(labelsize=labelsize)
ax[1].tick_params(labelsize=labelsize)
plt.tight_layout()
plt.savefig(Sampname+"_I_E(V_BE, V_BC)_and_I_B(V_BE, V_BC).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_E(V_BE, V_BC)_and_I_B(V_BE, V_BC).png", bbox_inches='tight')
#%% I_E(I_B) for different V_EC values
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"$|I_\mathrm{E}|$ (nA)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=abs(i_e[n,:]*1e9)
    v=v_ec[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".", s=point_size, label=r"$V_\mathrm{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(-v_ec)*1e3, vmax=np.max(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="out")
ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_I_E(I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_E(I_B).png", bbox_inches='tight')
#%% I_C(I_B) for different V_EC values
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=0.5))
ax.set_xlabel(r"$I_\mathrm{B}$ (nA)", size=size)
ax.set_ylabel(r"$I_\mathrm{C}$ (nA)", size=size)
ax.grid()
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
# ax.set_xlim(0, xmax=10) #(nA)
# ax.set_ylim(0, ymax=3000) #(nA)
n=0
NUM_COLORS = n_points
cm = plt.get_cmap(cb_colour)
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
while n<= (n_points-1):
    x=i_b[n,:]*1e9
    y=i_c[n,:]*1e9
    v=v_ec[n,0]*1e03
    #Order points by their x-value
    indexs_to_order_by = x.argsort()
    x_ordered = x[indexs_to_order_by]
    y_ordered = y[indexs_to_order_by]
    ax.scatter(x_ordered, y_ordered,marker=".",s=point_size, label=r"$V_\mathrm{EC}$=%.1f (mV)" % v)
    n+=1
# ax.legend(bbox_to_anchor=(0, -0.4),loc=3, ncol=5, borderaxespad=0.0)
sm = plt.cm.ScalarMappable(cmap=cm, norm=plt.Normalize(vmin=np.min(-v_ec)*1e3, vmax=np.max(-v_ec)*1e3))
cb=plt.colorbar(sm)
cb.ax.set_title(r"$V_\mathrm{CE}$ (mV)", size=size)
cb.ax.tick_params(labelsize=labelsize)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=7, which="major",direction="out")
ax.tick_params(width=tick_width, length=4, which="minor",direction="out")
ax.tick_params(labelsize=labelsize)
plt.savefig(Sampname+"_I_C(I_B).pdf", bbox_inches='tight') 
plt.savefig(Sampname+"_I_C(I_B).png", bbox_inches='tight')