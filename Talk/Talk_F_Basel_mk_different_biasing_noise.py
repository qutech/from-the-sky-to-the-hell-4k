# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 14:57:06 2021

@author: levon
"""


import qcodes as qc
from qcodes.loops import Loop, active_loop, active_data_set
#import qcodes.instrument_drivers.Lakeshore.Model_325 as ls #Temperature sensor
import os
#Magnet Power Supply missing here
from qcodes.tests.instrument_mocks import DummyInstrument
from qcodes.plots.pyqtgraph import QtPlot
from qcodes.plots.qcmatplotlib import MatPlot

from mpl_toolkits.mplot3d.axes3d import Axes3D
from qcodes.instrument.parameter import ManualParameter
from qcodes.instrument.parameter import Parameter
from qcodes import Station, load_or_create_experiment, \
    initialise_database, Measurement, load_by_run_spec, load_by_guid, initialise_or_create_database_at
from qcodes.dataset.plotting import plot_dataset
import numpy as np
import pandas as pd
import seaborn as sns
import math
import time
#matplotlib inline
#%matplotlib auto
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import fftpack
from scipy import signal
from scipy.constants import k
from scipy.constants import elementary_charge as e
import scipy.io
from scipy import integrate
from sklearn.linear_model import LinearRegression
from os import listdir
from os.path import isfile, join
#%% define global parameters
def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    #golden_ratio = (5**.5 - 1) / 2
    golden_ratio=1

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)
latex_width = 72.27*10
size=24
labelsize=22
tick_width=1.5
#%%
os.chdir("C:/Users/levon/Desktop/talk_plots/noise_F_Basel")
mypath="C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep"
p=os.listdir(mypath)
p.remove('Discard')
a=p[0]
b=p[1]
c=p[-1]
p=p[2:-1]
p.append(a)
p.append(b)
p.append(c)
del a,b,c
#p=p[5:9]
#p=p[-4:-1]
n=len(p)

gain=0.5e6
fig,ax = plt.subplots(figsize=set_size(latex_width, fraction=1))
ax.set_ylabel(r"Noise (A/$\mathrm{\sqrt{Hz}}$)", size=size)
ax.set_xlabel(r"$f$ (Hz)", size=size)
#ax.set_title("Noise spectral density of collecotor curren for different emmiter voltages", fontsize=size, fontname='serif', color="blue")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(2)
ax.tick_params(width=tick_width, length=12, which="major",direction="in")
ax.tick_params(width=tick_width, length=6, which="minor",direction="in")
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
#ax.text(0, 0.1, "Text label", fontsize=size, family="serif")
#formatter = mpl.ticker.ScalarFormatter(useMathText=True)
#ax.xaxis.set_major_formatter(formatter)
#ax.yaxis.set_major_formatter(formatter)
NUM_COLORS = n
cm = plt.get_cmap("viridis_r")
ax.set_prop_cycle(color=[cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for i in range(n-1):
    mat = scipy.io.loadmat("C:/Users/levon/Desktop/HBT noise/DATA/hbt_noise/biassweep/{0:s}".format( p[i] ) )
    FFT_data=(mat.get("spectra")["FFTdata"])[0,0].copy()
    FFT_data=FFT_data[:,0]
    FFT_freq=(mat.get("spectra")["FFTfreq"])[0,0].copy().T
    FFT_freq=FFT_freq[:,0]
    timestep=(mat.get("spectra")["timestep"])[0,0].copy()
    timestep=timestep[0,0]
    hardwareSampFreq=(mat.get("spectra")["hardwareSampFreq"])[0,0].copy()
    hardwareSampFreq=hardwareSampFreq[0,0]
    v_ec=(mat.get("userdata")["configvals"])[0,0].copy()
    v_ec=v_ec[0,1]*1000
    # FFT_data[np.where(FFT_freq>=500000)]=np.nan
    # FFT_data = FFT_data[~np.isnan(FFT_data)]
    # FFT_freq[np.where(FFT_freq>=500000)]=np.nan
    # FFT_freq = FFT_freq[~np.isnan(FFT_freq)]
    N=FFT_freq.size
    f_s=hardwareSampFreq #sampling frquency
    delta_f=f_s/N #resolution of the frequency spectrum
    B=f_s/2 #frequency spectrum bandwith, determined by the sampling theorem
    f=FFT_freq
    T=N/f_s
    S_omega_i_c=FFT_data
    ax.plot(f,np.sqrt(S_omega_i_c)/gain,lw=0.3, label=r"$V_\mathrm{EC}$=%.1f (mV)" % v_ec)
    Z_o=50
    T_N=S_omega_i_c*Z_o/k
    print("Noise temperature is {} Kelvin".format(T_N))
    # test_f=f.copy()
    # test_f[np.where(FFT_freq>=100e3)]=np.nan
    # test_f = test_f[~np.isnan(test_f)]
    # S_test=S_omega_i_c.copy()
    # S_test[np.where(FFT_freq>=100e3)]=np.nan
    # S_test = S_test[~np.isnan(S_test)]
    # S_test=np.nanmean(S_test[-250:])
    # ax.plot(test_f,np.sqrt(S_test*np.ones(test_f.shape))/gain, color="maroon")
    n+=1

ax.tick_params(axis='x', labelsize=labelsize)
ax.tick_params(axis='y', labelsize=labelsize)
xcoords = [100e3, 100e3]
for xc in xcoords:
    plt.axvline(x=xc, color="maroon",   linestyle="--")
leg=ax.legend(bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0.0, prop={'size': labelsize})
for line in leg.get_lines():
    line.set_linewidth(2)
plt.text(0.62, 0.8, "frequency cutoff", rotation=90, transform=ax.transAxes, verticalalignment='center', size=size, family="serif", color="maroon")

print("Do you want to save the plot?")
ans = input()
if ans == "1":
        plt.savefig("mK_collector_different_biased_noisev2.pdf", bbox_inches='tight') 
        plt.savefig("mK_collector_different_biased_noisev2.png", bbox_inches='tight')
if ans == "0": pass